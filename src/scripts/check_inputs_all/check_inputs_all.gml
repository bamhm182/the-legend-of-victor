if (state != "TELEPORT" && state != "ATTACK" && iframes <= 15) {
	if (state != "MOVING") {
		if (Input.up) state = "UP";
		if (Input.down) state = "DOWN";
		if (Input.left) state = "LEFT";
		if (Input.right) state = "RIGHT";
		if (!Input.up && !Input.down && !Input.left && !Input.right) state = "IDLE";
		
		if (attack_command == "X") {
			state = "ATTACK";
			move_blocked = true;
			did_attack = false;
		} else if (attack_command == "D" && Input.k_d) {
			state = "ATTACK";
			attack_command = "DD";
			move_blocked = true;
			did_attack = false;
		}
	}

	if (Input.k_x) {
		move_blocked = true;
		attack_command = "X"
	}
	
	if (Input.k_d && attack_command == "") {
		attack_command = "D";
	}
	
	if (global.state_time > 5 && Input.command) {
		state_switch(st_command);
		move_blocked = false;
	}
}

if (state != "ATTACK") {
	move_blocked = false;
}
/// state_switch(state)

global.last_state = global.state;
global.state_changed = true;
global.state = argument[0];
global.state_time = 0;
/// state_process(state)

if (global.state == 0) {
	global.state = argument[0];
}

script_execute(global.state);

if (!global.state_changed) global.state_time++;
global.state_changed = false;
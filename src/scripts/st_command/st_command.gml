if (instance_exists(obj_command_line)) {
	if (Input.enter) {
		if (!instance_exists(obj_help)) global.pause = false;
		run_vi_command(command_line.command);
		instance_destroy(obj_command_line);
	} else if (keyboard_string == "") {
		global.pause = false;
		state_switch(global.last_state);
		instance_destroy(obj_command_line);
	}
} else {
	command_line = instance_create_depth(32, 342, -200, obj_command_line);
	global.pause = true;
}
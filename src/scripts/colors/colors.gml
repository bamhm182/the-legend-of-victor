switch(argument[0]) {
	case "lightest": return make_color_rgb(155, 188, 15);
	case "light": return make_color_rgb(139, 172, 15);
	case "dark": return make_color_rgb(48, 98, 48);
	case "darkest": return make_color_rgb(15, 56, 15);
	default: return make_color_rgb(255,0,0);
}
/// run_vi_command(command)

command = string_delete(argument[0], 1, 1);
play_error = false;

state_switch(global.last_state);

switch (string_lower(argument[0])) {
	case ":0":
	case ":1":
	case ":2":
	case ":3":
	case ":4":
	case ":5":
	case ":6":
	case ":7":
	case ":8":
	case ":9":
	case ":10":
		if (instance_exists(obj_victor)) {
			with (instance_find(obj_victor,0)) {
				add_for_zero = 0;
				if (other.command = "0") add_for_zero = 32;
				desired_y = 16 + (real(string_digits(other.command)) * 32) + add_for_zero;
				if(!place_meeting(x, desired_y, obj_solid)) {
					teleport_y = desired_y;
					state = "TELEPORT";
				} 
			}
		} else {
			play_error = true;
		}
		break;
	case ":help":
		instance_create_depth(0,0,-50,obj_help);
		break;
	case ":q":
	case ":quit":
		if (global.pause) {
			global.pause = false;
		} else if (room == rm_title) {
			game_end();
		} else {
			state_switch(st_title);
			room_goto(rm_title);
		}
		break;
	case ":sound":
		global.sfx = !global.sfx;
		if (!global.sfx) {
			audio_stop_all();
			intro_played = false;
		}
		break;
	case ":cheat":
		if (instance_exists(obj_victor)) {
		global.cheat = !global.cheat;
		if (global.cheat && global.sfx) audio_play_sound(snd_cheat_activated,10,false);
		} else {
			play_error = true;
		}
		break;
	default: play_error = true break;
}

if (play_error && global.sfx) {
	audio_play_sound(snd_no_power,10,false);
}
{
    "id": "73f9b35a-4881-499d-b33b-f28232c618ab",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_victor_up",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 29,
    "bbox_left": 5,
    "bbox_right": 26,
    "bbox_top": 3,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "5b977713-f43b-4c3f-a57b-d9df72a373f5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "73f9b35a-4881-499d-b33b-f28232c618ab",
            "compositeImage": {
                "id": "37dbabc4-837c-4cd1-b79a-c4005794b498",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5b977713-f43b-4c3f-a57b-d9df72a373f5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b51071c3-d379-4cda-b6fa-bc43b40860fa",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5b977713-f43b-4c3f-a57b-d9df72a373f5",
                    "LayerId": "9171c6c6-162b-4e22-94d4-deb06055770e"
                }
            ]
        },
        {
            "id": "4b090509-a6fe-46f8-9b8a-4c412f2c801a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "73f9b35a-4881-499d-b33b-f28232c618ab",
            "compositeImage": {
                "id": "8acd5962-0d74-48d0-99d8-a299ebc7911c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4b090509-a6fe-46f8-9b8a-4c412f2c801a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2cf41949-11e4-459e-b89b-7d348ea1db4c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4b090509-a6fe-46f8-9b8a-4c412f2c801a",
                    "LayerId": "9171c6c6-162b-4e22-94d4-deb06055770e"
                }
            ]
        },
        {
            "id": "69c446f9-b1a9-4160-ac6a-dfe90bb2645b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "73f9b35a-4881-499d-b33b-f28232c618ab",
            "compositeImage": {
                "id": "4931b1ae-2e0d-4ca7-b87d-b157657e54cb",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "69c446f9-b1a9-4160-ac6a-dfe90bb2645b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0327ac51-467f-4355-b0f1-1e7043bac5df",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "69c446f9-b1a9-4160-ac6a-dfe90bb2645b",
                    "LayerId": "9171c6c6-162b-4e22-94d4-deb06055770e"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "9171c6c6-162b-4e22-94d4-deb06055770e",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "73f9b35a-4881-499d-b33b-f28232c618ab",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 16,
    "yorig": 16
}
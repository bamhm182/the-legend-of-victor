{
    "id": "4aa8a17d-891b-42d9-af2a-205884a2b507",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_victor_side",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 29,
    "bbox_left": 5,
    "bbox_right": 26,
    "bbox_top": 3,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "f645884a-611e-46df-9e3d-84339f7f8701",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4aa8a17d-891b-42d9-af2a-205884a2b507",
            "compositeImage": {
                "id": "2c2d3d3c-7a75-40ed-a199-4180ef60aa67",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f645884a-611e-46df-9e3d-84339f7f8701",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9aeb911b-a195-44d5-8934-daac08b72fe8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f645884a-611e-46df-9e3d-84339f7f8701",
                    "LayerId": "95adb4dc-28f2-4d09-bbda-686ef0322fe1"
                }
            ]
        },
        {
            "id": "9898f0e9-9f0b-43b1-aedb-636c262a1445",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4aa8a17d-891b-42d9-af2a-205884a2b507",
            "compositeImage": {
                "id": "8a243da3-437a-44c4-a66d-42a3a92fa3cd",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9898f0e9-9f0b-43b1-aedb-636c262a1445",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8bcec912-0898-449e-9266-b905593d6d1f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9898f0e9-9f0b-43b1-aedb-636c262a1445",
                    "LayerId": "95adb4dc-28f2-4d09-bbda-686ef0322fe1"
                }
            ]
        },
        {
            "id": "5f055adc-0096-4403-a1bc-9d8be768fbfb",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4aa8a17d-891b-42d9-af2a-205884a2b507",
            "compositeImage": {
                "id": "1d2b2645-0ad1-428c-bdbd-6327554ac8a5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5f055adc-0096-4403-a1bc-9d8be768fbfb",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "95331e3a-42c1-483b-85be-919d890ead9b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5f055adc-0096-4403-a1bc-9d8be768fbfb",
                    "LayerId": "95adb4dc-28f2-4d09-bbda-686ef0322fe1"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "95adb4dc-28f2-4d09-bbda-686ef0322fe1",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "4aa8a17d-891b-42d9-af2a-205884a2b507",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 7,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 16,
    "yorig": 16
}
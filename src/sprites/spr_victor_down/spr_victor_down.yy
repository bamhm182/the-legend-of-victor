{
    "id": "fa249f66-d62c-4c5d-85cd-aaf6146badd6",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_victor_down",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 29,
    "bbox_left": 5,
    "bbox_right": 26,
    "bbox_top": 3,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "c062d6db-be76-4694-a1ec-ba79a8044574",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fa249f66-d62c-4c5d-85cd-aaf6146badd6",
            "compositeImage": {
                "id": "acba2d40-ccfe-471d-98fc-955cfe5aa3b1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c062d6db-be76-4694-a1ec-ba79a8044574",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1bcf4e9d-f282-47fd-9bde-b4eaacacc48d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c062d6db-be76-4694-a1ec-ba79a8044574",
                    "LayerId": "74e89a2c-196c-4ffe-abc2-fbb1d432313c"
                }
            ]
        },
        {
            "id": "a570b1eb-a549-44b0-acf0-4e4c6fe2e574",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fa249f66-d62c-4c5d-85cd-aaf6146badd6",
            "compositeImage": {
                "id": "bbe25cb9-d9eb-4f3e-b614-f19dda4e1128",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a570b1eb-a549-44b0-acf0-4e4c6fe2e574",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "fa9f2df9-bade-4e7d-bc3a-efdf1eafe3ad",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a570b1eb-a549-44b0-acf0-4e4c6fe2e574",
                    "LayerId": "74e89a2c-196c-4ffe-abc2-fbb1d432313c"
                }
            ]
        },
        {
            "id": "b6661517-0f1f-46c2-b04f-956682c63a3c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fa249f66-d62c-4c5d-85cd-aaf6146badd6",
            "compositeImage": {
                "id": "a4a304d1-5ab7-409e-a006-c2f757d21f48",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b6661517-0f1f-46c2-b04f-956682c63a3c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "85b015af-5274-401d-ba54-92dc68376056",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b6661517-0f1f-46c2-b04f-956682c63a3c",
                    "LayerId": "74e89a2c-196c-4ffe-abc2-fbb1d432313c"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "74e89a2c-196c-4ffe-abc2-fbb1d432313c",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "fa249f66-d62c-4c5d-85cd-aaf6146badd6",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 16,
    "yorig": 16
}
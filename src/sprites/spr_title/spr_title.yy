{
    "id": "9aeca9ca-0229-486d-b7ca-6a3d3cb4be28",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_title",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 274,
    "bbox_left": 82,
    "bbox_right": 382,
    "bbox_top": 32,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "65e3ae62-41eb-4a5f-9a6e-54d11e67c256",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9aeca9ca-0229-486d-b7ca-6a3d3cb4be28",
            "compositeImage": {
                "id": "4cf5cfe7-bc2a-438d-84ca-eff4f922db4d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "65e3ae62-41eb-4a5f-9a6e-54d11e67c256",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7e922c8a-b9b0-4615-a708-9e757e18df72",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "65e3ae62-41eb-4a5f-9a6e-54d11e67c256",
                    "LayerId": "fcbbcd79-435a-4347-8af5-0366bbd952a3"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 288,
    "layers": [
        {
            "id": "fcbbcd79-435a-4347-8af5-0366bbd952a3",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "9aeca9ca-0229-486d-b7ca-6a3d3cb4be28",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 480,
    "xorig": 240,
    "yorig": 144
}
{
    "id": "c7a5280c-5549-4b39-b7d1-b7ac7be5459a",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_border",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 0,
    "bbox_right": 31,
    "bbox_top": 0,
    "bboxmode": 1,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "fbeec129-994f-473a-9910-71c889162c09",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c7a5280c-5549-4b39-b7d1-b7ac7be5459a",
            "compositeImage": {
                "id": "b47c6a61-c9fd-485b-a9a8-4418b146b717",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "fbeec129-994f-473a-9910-71c889162c09",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "df0c6d3e-8832-4eed-92e2-5e31d2d072f4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "fbeec129-994f-473a-9910-71c889162c09",
                    "LayerId": "d902299f-0b16-47ce-8579-e3ed0abac158"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "d902299f-0b16-47ce-8579-e3ed0abac158",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "c7a5280c-5549-4b39-b7d1-b7ac7be5459a",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 0,
    "yorig": 0
}
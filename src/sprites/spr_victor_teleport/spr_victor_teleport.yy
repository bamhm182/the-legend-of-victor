{
    "id": "391e159d-9c02-4b2c-b28e-60b19c65c656",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_victor_teleport",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 29,
    "bbox_left": 0,
    "bbox_right": 26,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "6b85bc8c-fa29-4840-aba2-ea51e5b5b4e9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "391e159d-9c02-4b2c-b28e-60b19c65c656",
            "compositeImage": {
                "id": "15971de2-6fc4-4239-a15a-04d07480a6f5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6b85bc8c-fa29-4840-aba2-ea51e5b5b4e9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8165e203-22b8-4f4c-a0ad-3722a6758161",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6b85bc8c-fa29-4840-aba2-ea51e5b5b4e9",
                    "LayerId": "a19b2204-9256-4560-8e7d-48312726a4e2"
                }
            ]
        },
        {
            "id": "97327a88-3984-4d57-b840-3d7d85d81d2a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "391e159d-9c02-4b2c-b28e-60b19c65c656",
            "compositeImage": {
                "id": "b0e007e6-30e2-4563-a725-39f6aa7ff6cb",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "97327a88-3984-4d57-b840-3d7d85d81d2a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "935f080d-ec47-4b4b-8e82-f90849bddaf4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "97327a88-3984-4d57-b840-3d7d85d81d2a",
                    "LayerId": "a19b2204-9256-4560-8e7d-48312726a4e2"
                }
            ]
        },
        {
            "id": "19884c2d-1827-442a-b420-b39d1ba28745",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "391e159d-9c02-4b2c-b28e-60b19c65c656",
            "compositeImage": {
                "id": "2877c3a6-14a0-4e7d-b886-49e71dfe2ce7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "19884c2d-1827-442a-b420-b39d1ba28745",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "93458990-9c20-4fc3-a93e-141e15d76546",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "19884c2d-1827-442a-b420-b39d1ba28745",
                    "LayerId": "a19b2204-9256-4560-8e7d-48312726a4e2"
                }
            ]
        },
        {
            "id": "c1eba1c6-a4e2-4edd-9aa7-31e09a882031",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "391e159d-9c02-4b2c-b28e-60b19c65c656",
            "compositeImage": {
                "id": "794cedae-6a15-4b47-b53d-f199b7930af1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c1eba1c6-a4e2-4edd-9aa7-31e09a882031",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5c65d4b9-b044-44d5-b973-2e254cd965dd",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c1eba1c6-a4e2-4edd-9aa7-31e09a882031",
                    "LayerId": "a19b2204-9256-4560-8e7d-48312726a4e2"
                }
            ]
        },
        {
            "id": "bfeb739e-a2dc-4959-bdf7-d5f111d666e3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "391e159d-9c02-4b2c-b28e-60b19c65c656",
            "compositeImage": {
                "id": "a8572c6a-531e-4129-a277-293c97ae5366",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "bfeb739e-a2dc-4959-bdf7-d5f111d666e3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "bcf20dff-96ec-4524-8ab4-02e816125995",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "bfeb739e-a2dc-4959-bdf7-d5f111d666e3",
                    "LayerId": "a19b2204-9256-4560-8e7d-48312726a4e2"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "a19b2204-9256-4560-8e7d-48312726a4e2",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "391e159d-9c02-4b2c-b28e-60b19c65c656",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 16,
    "yorig": 16
}
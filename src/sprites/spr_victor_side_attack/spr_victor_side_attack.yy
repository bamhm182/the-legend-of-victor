{
    "id": "7a3df832-e524-4fed-bc50-5e43af4dfda6",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_victor_side_attack",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 29,
    "bbox_left": 5,
    "bbox_right": 26,
    "bbox_top": 3,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "d79fcc0e-9359-49c5-9724-1d4e8e3c4aa9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7a3df832-e524-4fed-bc50-5e43af4dfda6",
            "compositeImage": {
                "id": "e97150b0-fdd9-4efd-9c3c-d66a13a06998",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d79fcc0e-9359-49c5-9724-1d4e8e3c4aa9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "766cab8e-cdaa-4217-8b1c-a3490066ffe8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d79fcc0e-9359-49c5-9724-1d4e8e3c4aa9",
                    "LayerId": "796a6c64-e6fc-403a-87b3-4d440c510c70"
                }
            ]
        },
        {
            "id": "5c95d966-31bf-4742-b448-dacd60751625",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7a3df832-e524-4fed-bc50-5e43af4dfda6",
            "compositeImage": {
                "id": "0fe3ffb2-35f8-4b97-8855-6d9c4117ef30",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5c95d966-31bf-4742-b448-dacd60751625",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "59dbfd73-4426-4b81-ba12-090b0ca07314",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5c95d966-31bf-4742-b448-dacd60751625",
                    "LayerId": "796a6c64-e6fc-403a-87b3-4d440c510c70"
                }
            ]
        },
        {
            "id": "fb84a492-a7ee-4d57-b013-a6d2a83a1443",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7a3df832-e524-4fed-bc50-5e43af4dfda6",
            "compositeImage": {
                "id": "70201306-0e7f-4495-9da1-37a6f900909b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "fb84a492-a7ee-4d57-b013-a6d2a83a1443",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a2affefa-f225-42fc-811a-be491c71f749",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "fb84a492-a7ee-4d57-b013-a6d2a83a1443",
                    "LayerId": "796a6c64-e6fc-403a-87b3-4d440c510c70"
                }
            ]
        },
        {
            "id": "a3e95e01-35fa-456f-8bde-026892a5fc61",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7a3df832-e524-4fed-bc50-5e43af4dfda6",
            "compositeImage": {
                "id": "0e7bf695-c812-4779-9b25-1bf0a6eca091",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a3e95e01-35fa-456f-8bde-026892a5fc61",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a5b62b73-c2fb-464e-afc5-752ae9438ec0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a3e95e01-35fa-456f-8bde-026892a5fc61",
                    "LayerId": "796a6c64-e6fc-403a-87b3-4d440c510c70"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "796a6c64-e6fc-403a-87b3-4d440c510c70",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "7a3df832-e524-4fed-bc50-5e43af4dfda6",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 5,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 16,
    "yorig": 16
}
{
    "id": "6174c1ad-c9eb-4798-9fe5-0be0a401e7a8",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_smoke",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 27,
    "bbox_left": 2,
    "bbox_right": 28,
    "bbox_top": 2,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "4befaa1e-c56b-4eb0-927f-f9acf7d533fc",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6174c1ad-c9eb-4798-9fe5-0be0a401e7a8",
            "compositeImage": {
                "id": "5d6bc2f2-0cee-48db-853f-cd2fa5e1b303",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4befaa1e-c56b-4eb0-927f-f9acf7d533fc",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7aeac4fb-d53f-4afb-8014-f49e2d1b5ff5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4befaa1e-c56b-4eb0-927f-f9acf7d533fc",
                    "LayerId": "d00050d0-0e3b-497d-a217-ef267c456176"
                }
            ]
        },
        {
            "id": "d35fe4da-5e98-4952-987a-3034161a01b2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6174c1ad-c9eb-4798-9fe5-0be0a401e7a8",
            "compositeImage": {
                "id": "ad537c44-6b5a-470e-97c3-990406a1333b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d35fe4da-5e98-4952-987a-3034161a01b2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0d42fac3-d76f-4184-b79d-dfa31a08a448",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d35fe4da-5e98-4952-987a-3034161a01b2",
                    "LayerId": "d00050d0-0e3b-497d-a217-ef267c456176"
                }
            ]
        },
        {
            "id": "e2e985c4-e9bf-4d0e-9167-701b2fef0dc8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6174c1ad-c9eb-4798-9fe5-0be0a401e7a8",
            "compositeImage": {
                "id": "509101df-4802-4e55-85df-be73e1d6638b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e2e985c4-e9bf-4d0e-9167-701b2fef0dc8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "19b45b56-9b53-4bc0-a795-8da234ddff0b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e2e985c4-e9bf-4d0e-9167-701b2fef0dc8",
                    "LayerId": "d00050d0-0e3b-497d-a217-ef267c456176"
                }
            ]
        },
        {
            "id": "60a79a4b-7d03-47c3-ad94-d2a778e4aeca",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6174c1ad-c9eb-4798-9fe5-0be0a401e7a8",
            "compositeImage": {
                "id": "1fca8d8f-fdfb-4d26-aa39-00a02922f5fa",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "60a79a4b-7d03-47c3-ad94-d2a778e4aeca",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "069270c2-13fb-46ab-91c3-f857b792c79d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "60a79a4b-7d03-47c3-ad94-d2a778e4aeca",
                    "LayerId": "d00050d0-0e3b-497d-a217-ef267c456176"
                }
            ]
        },
        {
            "id": "71132dd3-6600-444e-a019-ff14ac1ec271",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6174c1ad-c9eb-4798-9fe5-0be0a401e7a8",
            "compositeImage": {
                "id": "fa6ea394-fbe7-4b67-a02d-675e5759bacc",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "71132dd3-6600-444e-a019-ff14ac1ec271",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e2c76c4c-5022-49ea-8663-da6f1f7ad5af",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "71132dd3-6600-444e-a019-ff14ac1ec271",
                    "LayerId": "d00050d0-0e3b-497d-a217-ef267c456176"
                }
            ]
        },
        {
            "id": "d57a536d-a641-4cac-b901-bb9b276b053d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6174c1ad-c9eb-4798-9fe5-0be0a401e7a8",
            "compositeImage": {
                "id": "c131d003-ceaf-4354-ae2a-9b48ff742ad1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d57a536d-a641-4cac-b901-bb9b276b053d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e005262e-5b71-4713-ab7a-c6ec9b60783a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d57a536d-a641-4cac-b901-bb9b276b053d",
                    "LayerId": "d00050d0-0e3b-497d-a217-ef267c456176"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "d00050d0-0e3b-497d-a217-ef267c456176",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "6174c1ad-c9eb-4798-9fe5-0be0a401e7a8",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": true,
    "playbackSpeed": 6,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 16,
    "yorig": 16
}
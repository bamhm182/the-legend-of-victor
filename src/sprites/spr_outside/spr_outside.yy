{
    "id": "62552746-c1ab-4b43-a18e-7a8258487c63",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_outside",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 95,
    "bbox_left": 32,
    "bbox_right": 191,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "ceda8399-ca8d-42bf-a478-6d8fbeed2828",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "62552746-c1ab-4b43-a18e-7a8258487c63",
            "compositeImage": {
                "id": "1ae7380a-0112-43a0-b270-7e5ce368bf42",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ceda8399-ca8d-42bf-a478-6d8fbeed2828",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6f7aa732-b532-4b69-873a-17fc420679b7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ceda8399-ca8d-42bf-a478-6d8fbeed2828",
                    "LayerId": "56c4bd2d-a302-40dc-b4d0-b5f1ed05d606"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 96,
    "layers": [
        {
            "id": "56c4bd2d-a302-40dc-b4d0-b5f1ed05d606",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "62552746-c1ab-4b43-a18e-7a8258487c63",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 192,
    "xorig": 0,
    "yorig": 0
}
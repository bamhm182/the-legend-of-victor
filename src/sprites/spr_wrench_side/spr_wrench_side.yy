{
    "id": "1bd34183-9973-40e9-a9d5-522b0da3d728",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_wrench_side",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 30,
    "bbox_left": 0,
    "bbox_right": 28,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "ee8b3c2f-e62b-4b31-b7df-8739f64ea8b6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1bd34183-9973-40e9-a9d5-522b0da3d728",
            "compositeImage": {
                "id": "c9f9d185-1365-4ab5-8a10-75787d276ebc",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ee8b3c2f-e62b-4b31-b7df-8739f64ea8b6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "de940706-7839-47c9-96d8-6b9c15f6fde8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ee8b3c2f-e62b-4b31-b7df-8739f64ea8b6",
                    "LayerId": "d1063db0-7f5c-4188-9232-70ac7f3c15d6"
                }
            ]
        },
        {
            "id": "1058ecd2-4de4-425f-b755-14762a89fac0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1bd34183-9973-40e9-a9d5-522b0da3d728",
            "compositeImage": {
                "id": "4dce85eb-1774-4aee-b2b3-64e0b00e9e32",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1058ecd2-4de4-425f-b755-14762a89fac0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d46bfc16-2a14-4a3f-b09c-59c0c2485c1b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1058ecd2-4de4-425f-b755-14762a89fac0",
                    "LayerId": "d1063db0-7f5c-4188-9232-70ac7f3c15d6"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "d1063db0-7f5c-4188-9232-70ac7f3c15d6",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "1bd34183-9973-40e9-a9d5-522b0da3d728",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": true,
    "playbackSpeed": 6,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 16,
    "yorig": 16
}
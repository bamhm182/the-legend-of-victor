{
    "id": "96990f9f-6831-4bd4-926c-166e541ead16",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_board",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 191,
    "bbox_left": 0,
    "bbox_right": 447,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "89bc2ac4-85d5-4b93-a087-8e99d8819a82",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "96990f9f-6831-4bd4-926c-166e541ead16",
            "compositeImage": {
                "id": "4e2d98b0-c24d-4d3d-b42b-5aa1052fd225",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "89bc2ac4-85d5-4b93-a087-8e99d8819a82",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b4fc35f1-745f-4da1-acf5-e47f6e45bcd7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "89bc2ac4-85d5-4b93-a087-8e99d8819a82",
                    "LayerId": "b8a742b1-e655-400c-9e11-8f2ec0a2e6c3"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 192,
    "layers": [
        {
            "id": "b8a742b1-e655-400c-9e11-8f2ec0a2e6c3",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "96990f9f-6831-4bd4-926c-166e541ead16",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": true,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 448,
    "xorig": 224,
    "yorig": 96
}
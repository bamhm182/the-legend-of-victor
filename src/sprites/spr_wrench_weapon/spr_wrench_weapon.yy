{
    "id": "36866625-5480-47e6-9f96-795798dd57c5",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_wrench_weapon",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 15,
    "bbox_left": 0,
    "bbox_right": 15,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "a2df4abd-5daa-4cea-836a-3988b55d95d3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "36866625-5480-47e6-9f96-795798dd57c5",
            "compositeImage": {
                "id": "6e91953b-2ddf-4392-9264-a6885301fbed",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a2df4abd-5daa-4cea-836a-3988b55d95d3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ae705fc7-4118-4979-a651-6a05e6c3ac7d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a2df4abd-5daa-4cea-836a-3988b55d95d3",
                    "LayerId": "e45e4ad5-d2bc-45ef-9ea9-0336487c799c"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "e45e4ad5-d2bc-45ef-9ea9-0336487c799c",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "36866625-5480-47e6-9f96-795798dd57c5",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 8
}
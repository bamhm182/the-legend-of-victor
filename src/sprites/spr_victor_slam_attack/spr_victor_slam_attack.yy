{
    "id": "8d3d4901-a9a0-423f-8926-a8961d1220cd",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_victor_slam_attack",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 30,
    "bbox_left": 5,
    "bbox_right": 26,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "f8a89416-8dda-4761-8cbe-3cbec419616a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8d3d4901-a9a0-423f-8926-a8961d1220cd",
            "compositeImage": {
                "id": "0172693b-2663-4494-af16-7ec33ba96bb1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f8a89416-8dda-4761-8cbe-3cbec419616a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e6214a98-a2ad-4971-aef4-d6dbf8d982fd",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f8a89416-8dda-4761-8cbe-3cbec419616a",
                    "LayerId": "4472e3f3-1b3b-4416-802a-7494f6ace5fb"
                }
            ]
        },
        {
            "id": "be579a82-1a68-4f85-b791-b75c8723c1ea",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8d3d4901-a9a0-423f-8926-a8961d1220cd",
            "compositeImage": {
                "id": "2c74c09f-14b2-4465-88db-b0461472d041",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "be579a82-1a68-4f85-b791-b75c8723c1ea",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "bb114ea7-1f36-4126-ac5c-0e073f8e2dfb",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "be579a82-1a68-4f85-b791-b75c8723c1ea",
                    "LayerId": "4472e3f3-1b3b-4416-802a-7494f6ace5fb"
                }
            ]
        },
        {
            "id": "311f3d3f-d61b-4918-b4d4-069a2c0f8eae",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8d3d4901-a9a0-423f-8926-a8961d1220cd",
            "compositeImage": {
                "id": "48717dfe-ba68-4a98-9d97-ab4e9e04e2ae",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "311f3d3f-d61b-4918-b4d4-069a2c0f8eae",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9c869f38-6f57-4707-a4c1-687efa646330",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "311f3d3f-d61b-4918-b4d4-069a2c0f8eae",
                    "LayerId": "4472e3f3-1b3b-4416-802a-7494f6ace5fb"
                }
            ]
        },
        {
            "id": "fa0f1b6c-9c6a-417b-a7f5-a8acb921a5c6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8d3d4901-a9a0-423f-8926-a8961d1220cd",
            "compositeImage": {
                "id": "73f39360-8612-4d6f-bf23-cc5fefc7cdb8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "fa0f1b6c-9c6a-417b-a7f5-a8acb921a5c6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "515ef412-51ef-4237-856a-86c661b17337",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "fa0f1b6c-9c6a-417b-a7f5-a8acb921a5c6",
                    "LayerId": "4472e3f3-1b3b-4416-802a-7494f6ace5fb"
                }
            ]
        },
        {
            "id": "68446afe-d7f1-42c8-b60f-c4c17adc6efa",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8d3d4901-a9a0-423f-8926-a8961d1220cd",
            "compositeImage": {
                "id": "c03effeb-3da0-43b1-85ba-95c87c27fe1b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "68446afe-d7f1-42c8-b60f-c4c17adc6efa",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "28e5247c-e5fc-4a01-a39e-224ea2fdcc1b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "68446afe-d7f1-42c8-b60f-c4c17adc6efa",
                    "LayerId": "4472e3f3-1b3b-4416-802a-7494f6ace5fb"
                }
            ]
        },
        {
            "id": "6c2bdbed-dd59-4477-ac07-2d4e8acf354d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8d3d4901-a9a0-423f-8926-a8961d1220cd",
            "compositeImage": {
                "id": "9f738cbe-c11b-492e-9d62-199d30a204f2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6c2bdbed-dd59-4477-ac07-2d4e8acf354d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5a073000-357f-4c13-bf73-db280622f9d4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6c2bdbed-dd59-4477-ac07-2d4e8acf354d",
                    "LayerId": "4472e3f3-1b3b-4416-802a-7494f6ace5fb"
                }
            ]
        },
        {
            "id": "fb67e987-5efd-4f91-90c3-5e2c90ad8b17",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8d3d4901-a9a0-423f-8926-a8961d1220cd",
            "compositeImage": {
                "id": "3d836f9c-0cba-42aa-b91c-8c595c851466",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "fb67e987-5efd-4f91-90c3-5e2c90ad8b17",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f73a2825-66b2-442c-a70b-f3be7a240097",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "fb67e987-5efd-4f91-90c3-5e2c90ad8b17",
                    "LayerId": "4472e3f3-1b3b-4416-802a-7494f6ace5fb"
                }
            ]
        },
        {
            "id": "e8bec842-46ea-4ce4-af51-ef116acf2ad9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8d3d4901-a9a0-423f-8926-a8961d1220cd",
            "compositeImage": {
                "id": "b72bf66b-2da9-44dc-8ad4-e7a3544f2a56",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e8bec842-46ea-4ce4-af51-ef116acf2ad9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5931784f-d188-448b-9779-7be75f3bc341",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e8bec842-46ea-4ce4-af51-ef116acf2ad9",
                    "LayerId": "4472e3f3-1b3b-4416-802a-7494f6ace5fb"
                }
            ]
        },
        {
            "id": "45d72bfd-668e-4dac-a1f6-17085d566940",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8d3d4901-a9a0-423f-8926-a8961d1220cd",
            "compositeImage": {
                "id": "0079f47f-f404-4979-949c-5976e1d770af",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "45d72bfd-668e-4dac-a1f6-17085d566940",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "da06eaea-7863-44b0-8c67-8a49e347e809",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "45d72bfd-668e-4dac-a1f6-17085d566940",
                    "LayerId": "4472e3f3-1b3b-4416-802a-7494f6ace5fb"
                }
            ]
        },
        {
            "id": "14a34257-89e0-422d-ba8b-1c0c6cfe354e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8d3d4901-a9a0-423f-8926-a8961d1220cd",
            "compositeImage": {
                "id": "ed8aab78-2b70-42da-82ae-82859176112d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "14a34257-89e0-422d-ba8b-1c0c6cfe354e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "bab625be-eae8-4a46-a720-b3f72fbdeacc",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "14a34257-89e0-422d-ba8b-1c0c6cfe354e",
                    "LayerId": "4472e3f3-1b3b-4416-802a-7494f6ace5fb"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "4472e3f3-1b3b-4416-802a-7494f6ace5fb",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "8d3d4901-a9a0-423f-8926-a8961d1220cd",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": true,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 16,
    "yorig": 16
}
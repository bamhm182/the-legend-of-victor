{
    "id": "3d32412b-0677-4790-863e-1a27ed649a37",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_command_line",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 0,
    "bbox_right": 575,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "716bb101-a6a7-4410-a1e7-d90393fea2b1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3d32412b-0677-4790-863e-1a27ed649a37",
            "compositeImage": {
                "id": "f3408dc2-8c98-4189-9fb1-3e58b2f867aa",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "716bb101-a6a7-4410-a1e7-d90393fea2b1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "95479872-ac4f-45d5-b744-e118f42b9697",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "716bb101-a6a7-4410-a1e7-d90393fea2b1",
                    "LayerId": "7f3762ad-d0cb-4eb4-8780-e08bedc3e49a"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "7f3762ad-d0cb-4eb4-8780-e08bedc3e49a",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "3d32412b-0677-4790-863e-1a27ed649a37",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 576,
    "xorig": 0,
    "yorig": 0
}
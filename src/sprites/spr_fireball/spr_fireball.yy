{
    "id": "c9955084-c0b9-4dd3-88a7-80f72f35f476",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_fireball",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 0,
    "bbox_right": 31,
    "bbox_top": 3,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "7e2e9f65-bd9d-4895-8def-f8467a049704",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c9955084-c0b9-4dd3-88a7-80f72f35f476",
            "compositeImage": {
                "id": "8a855cdb-5506-4f65-8694-ebb01fbe4d59",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7e2e9f65-bd9d-4895-8def-f8467a049704",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f012a1fc-8a01-40f1-a090-9ac0e5f5fe63",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7e2e9f65-bd9d-4895-8def-f8467a049704",
                    "LayerId": "6ab9ad02-42c1-461c-a90c-0ace08522e41"
                }
            ]
        },
        {
            "id": "6db72261-3984-4c58-b6a9-bd40861b035b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c9955084-c0b9-4dd3-88a7-80f72f35f476",
            "compositeImage": {
                "id": "c6b61d01-e9d3-4773-928f-2763db2db91e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6db72261-3984-4c58-b6a9-bd40861b035b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4488384e-eda3-41c7-a95d-bdb7b2f8a999",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6db72261-3984-4c58-b6a9-bd40861b035b",
                    "LayerId": "6ab9ad02-42c1-461c-a90c-0ace08522e41"
                }
            ]
        },
        {
            "id": "cb6e92b6-f4eb-49f7-ad50-d1c9aec8c9bf",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c9955084-c0b9-4dd3-88a7-80f72f35f476",
            "compositeImage": {
                "id": "3658e9ab-4e28-49e5-b589-f0d1fe59ce8a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "cb6e92b6-f4eb-49f7-ad50-d1c9aec8c9bf",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ca16086b-837c-4401-8a42-a59150dffb95",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "cb6e92b6-f4eb-49f7-ad50-d1c9aec8c9bf",
                    "LayerId": "6ab9ad02-42c1-461c-a90c-0ace08522e41"
                }
            ]
        },
        {
            "id": "33bdbdb9-8688-4b8e-b2b4-b771ab3209c3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c9955084-c0b9-4dd3-88a7-80f72f35f476",
            "compositeImage": {
                "id": "4695522a-5598-460a-b3d7-8b2a6689af2c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "33bdbdb9-8688-4b8e-b2b4-b771ab3209c3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "830681a2-d34a-40ce-9f63-9ed2d9fedb2e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "33bdbdb9-8688-4b8e-b2b4-b771ab3209c3",
                    "LayerId": "6ab9ad02-42c1-461c-a90c-0ace08522e41"
                }
            ]
        },
        {
            "id": "2e525e93-d1ab-4723-9ca8-3fc75df38d36",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c9955084-c0b9-4dd3-88a7-80f72f35f476",
            "compositeImage": {
                "id": "01d803cc-0b90-4ecf-88e8-ec6663927741",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2e525e93-d1ab-4723-9ca8-3fc75df38d36",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "aa0bcb1f-0c40-44e9-a47c-b4a63452e4be",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2e525e93-d1ab-4723-9ca8-3fc75df38d36",
                    "LayerId": "6ab9ad02-42c1-461c-a90c-0ace08522e41"
                }
            ]
        },
        {
            "id": "44c1672d-4677-4345-8772-180a48726bb8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c9955084-c0b9-4dd3-88a7-80f72f35f476",
            "compositeImage": {
                "id": "48ef804d-c043-4c43-a045-3b5f2a706b92",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "44c1672d-4677-4345-8772-180a48726bb8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a333bf93-5e23-423a-8dc6-788e1006ad92",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "44c1672d-4677-4345-8772-180a48726bb8",
                    "LayerId": "6ab9ad02-42c1-461c-a90c-0ace08522e41"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "6ab9ad02-42c1-461c-a90c-0ace08522e41",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "c9955084-c0b9-4dd3-88a7-80f72f35f476",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 16,
    "yorig": 16
}
/// @description Wrench Bite

if (iframes == 0 && other.hp > 0) {
	state = "IDLE";

	hp -= other.damage;

	if (global.sfx) audio_play_sound(snd_bite,10,false);
	
	x = (x & ~31) + 16;
	y = (y & ~31) + 16;


	iframes = 30;
}
{
    "id": "6cc53553-4744-4f81-b714-1cac90670a82",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_victor",
    "eventList": [
        {
            "id": "2cc53781-b464-4727-8ba8-ffa66c8159d1",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "6cc53553-4744-4f81-b714-1cac90670a82"
        },
        {
            "id": "652ec79b-0cd3-4c79-acd7-2501c3edde7d",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "6cc53553-4744-4f81-b714-1cac90670a82"
        },
        {
            "id": "9c73b08d-a9d2-4f88-91e5-cfbaa003da09",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "6cc53553-4744-4f81-b714-1cac90670a82"
        },
        {
            "id": "ef702bb2-c491-4ab7-ad00-554b9b20d170",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "303fc5fb-60fe-4446-91de-ed41f2b8225c",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "6cc53553-4744-4f81-b714-1cac90670a82"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "fa249f66-d62c-4c5d-85cd-aaf6146badd6",
    "visible": true
}
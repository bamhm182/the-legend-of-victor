/// @description Draw Health & Power

if (iframes == 0 || iframes mod 3 != 0) draw_self();

draw_set_valign(fa_middle);
draw_set_halign(fa_center);
draw_set_font(fnt_gb);
draw_set_color(colors("lightest"));
draw_text_transformed(544, 40, "HEALTH",.5,.5,0);
draw_text_transformed(608, 40, "POWER",.5,.5,0);

if (hp == max_hp) {
	draw_set_color(colors("lightest"));
} else {
	draw_set_color(colors("dark"));
}
draw_rectangle(528,30*(1 - hp/max_hp),558,30,false);

if (pwr == max_pwr) {
	draw_set_color(colors("lightest"));
} else {
	draw_set_color(colors("dark"));
}
draw_rectangle(592,30*(1 - pwr/max_pwr),622,30,false);

draw_sprite(spr_power_meter, 0, 544, 16);
draw_sprite(spr_power_meter, 0, 608, 16);
/// @description Movement

if (hp <= 0) instance_destroy();

if (global.pause) exit;

anim_speed = 0.5;

check_inputs_all();

if (global.cheat) {
	hp = max_hp;
	pwr = max_pwr;
}

switch(state) {
	case "IDLE": {
		image_speed = 0;
		sprite_index = sprite_walk;
		image_index = 0;
		vx = 0;
		vy = 0;
		break;
	}
	case "UP": {
		image_speed = anim_speed;
		sprite_walk = spr_victor_up;
		sprite_index = sprite_walk;
		image_xscale = 1;
		vx = 0;
		vy = -player_speed;
		break;
	}
	case "DOWN": {
		image_speed = anim_speed;
		sprite_walk = spr_victor_down;
		sprite_index = sprite_walk;
		image_xscale = 1;
		vx = 0;
		vy = player_speed;
		break;
	}
	case "LEFT": {
		image_speed = anim_speed*2;
		sprite_walk = spr_victor_side;
		sprite_index = sprite_walk;
		image_xscale = -1;
		vx = -player_speed;
		vy = 0;
		break;
	}
	case "RIGHT": {
		image_speed = anim_speed*2;
		sprite_walk = spr_victor_side;
		sprite_index = sprite_walk;
		image_xscale = 1;
		vx = player_speed;
		vy = 0;
		break;
	}
	case "MOVING": {
		attack_command = "";
		x += vx;
		y += vy;
		if (x == new_x && y == new_y) state = "IDLE";
		break;
	}
	case "ATTACK": {
		show_debug_message(pwr);
		switch(attack_command) {
			case "DD":
				if (pwr >= 10 || attack_approved) {
					if !(attack_approved) {
						if (global.sfx) audio_play_sound(snd_dd_jump,10,false);
						pwr -= 10;
						attack_approved = true;
						left_fire_x = x;
						right_fire_x = x;
					}
					image_speed = anim_speed*4;
					sprite_walk = spr_victor_slam_attack;
					sprite_index = sprite_walk;
					image_xscale = 1;
					vx = 0;
					vy = 0;
					if (image_index > image_number - 1) image_speed = 0;
					if (image_speed = 0) {
						if (left_fire_x mod 32 == 0) {
							instance_create_depth(left_fire_x-16, y, 0, obj_fireball);
						}
						if (right_fire_x mod 32 == 0) {
							instance_create_depth(right_fire_x+16, y, 0, obj_fireball);
						}
						left_fire_x-=8;
						right_fire_x+=8;
					}
					if (left_fire_x < 0 && right_fire_x > 640) {
						state = "IDLE";
						attack_approved = false;
						attack_command = "";
					}
				} else {
					if (global.sfx) audio_play_sound(snd_no_power,10,false);
					state = "IDLE";
					attack_command = "";
				}
				break;
			case "X":
				if (pwr >= 2 || attack_approved) {
					if (!attack_approved) {
						pwr -= 2;
						attack_approved = true;
					}
					image_speed = anim_speed * 5;
					sprite_walk = spr_victor_side_attack;
					sprite_index = sprite_walk;
					image_xscale = 1;
					vx=0;
					vy=0;
					if (image_index > 2 && !did_attack) {
						instance_create_depth(x+32,y,-10,obj_fireball);
						did_attack = true;
					}
					if (image_index > image_number - 1) {
						state = "IDLE";
						attack_approved = false;
						attack_command = "";
					}
				} else {
					if (global.sfx) audio_play_sound(snd_no_power,10,false);
					state = "IDLE";
					attack_command = "";
				}
				break;
		}

		break;
	}
	case "TELEPORT": {
		if (pwr >= 5 || teleport_approved) {
			if (!teleport_approved) {
				pwr -= 5;
				teleport_approved = true;
				if (global.sfx) audio_play_sound(snd_teleport,10,false);
			}
			if (image_index == 0) image_speed = anim_speed*5;
			sprite_walk = spr_victor_teleport;
			sprite_index = sprite_walk;
			image_xscale = 1;
			if (image_index > image_number - 1) {
				y = teleport_y;
				x = (x & ~31) + 16;
				teleport_y = 0;
				image_speed*=-1;
			}
			if (teleport_y == 0 && image_index == 0) {
					state = "IDLE";
					sprite_walk = spr_victor_down;
					teleport_approved = false;
			}
		} else {
			if (global.sfx) audio_play_sound(snd_no_power,10,false);
			state = "IDLE";
		}
	}
}


if (state != "MOVING" && state != "IDLE" && state != "TELEPORT" && !move_blocked && (vx != 0 || vy != 0)) {
	new_x = x + (32 * sign(vx));
	new_y = y + (32 * sign(vy));
	if (!place_meeting(new_x, new_y, obj_solid)) state = "MOVING";
}

if (pwr < max_pwr && pwr_timeout <= 0) {
	pwr++;
	pwr_timeout = 90;
}
pwr_timeout--;

if (hp < max_hp && hp_timeout <= 0) {
	hp++;
	hp_timeout = 30;
}
hp_timeout--;

if (iframes > 0) {
	iframes--;
}

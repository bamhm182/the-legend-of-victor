/// @description Init

vx = 0;
vy = 0;
teleport_y = 0;
player_speed = 2;
iframes = 0;

state = "IDLE";

sprite_walk = spr_victor_down;

move_blocked = false;
attack_command = "";

max_hp = 50;
hp = max_hp;
hp_timeout = 30;

max_pwr = 10;
pwr = max_pwr;
pwr_timeout = 90;

did_attack = false;

attack_approved = false;
teleport_approved = false;
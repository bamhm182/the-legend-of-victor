/// @description Display Game Over


// Check for Game Over
if (!instance_exists(obj_victor) && !instance_exists(obj_gameover)) {
	instance_create_depth(320, 192, -200, obj_gameover);
}

// Spawn Wrench Monsters
if (irandom(100) < 1 && instance_exists(obj_victor)) {
	left_side = irandom(1);
	if (left_side) {
		spawn_x = 16;
		spawn_y = ceil(random_range(0,10)) * 32 + 16;
	} else {
		spawn_x = 624;
		spawn_y = ceil(random_range(1,10)) * 32 + 16;
	}

	if (!place_meeting(spawn_x, spawn_y, obj_enemy)) {
		instance_create_depth(spawn_x, spawn_y, 0, obj_wrench);
	}
}
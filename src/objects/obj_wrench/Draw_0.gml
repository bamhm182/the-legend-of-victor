/// @description Hide Behind Bush

if (place_meeting(x, y, obj_solid)) {
	rand_move = 0;
	x_start = 0;
	if (x > 320) x_start = 608;
	if (irandom(25) == 0) rand_move = ceil(random_range(-2,2));
	draw_tile(til_outside, tile_data, 0, x_start+rand_move, y-16+rand_move);
}
	
if (x > 16 && x < 624) draw_self();
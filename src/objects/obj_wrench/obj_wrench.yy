{
    "id": "303fc5fb-60fe-4446-91de-ed41f2b8225c",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_wrench",
    "eventList": [
        {
            "id": "4d74e6a2-15b8-46fb-bb89-f726164a5847",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "303fc5fb-60fe-4446-91de-ed41f2b8225c"
        },
        {
            "id": "09a035ef-178c-46c1-ac11-39f02d7501c3",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "303fc5fb-60fe-4446-91de-ed41f2b8225c"
        },
        {
            "id": "8b92b038-146e-45d4-bff7-8d557caf2b4f",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "303fc5fb-60fe-4446-91de-ed41f2b8225c"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "bd154384-9d70-45c0-89f2-68532a744680",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "1bd34183-9973-40e9-a9d5-522b0da3d728",
    "visible": true
}
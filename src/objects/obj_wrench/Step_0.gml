/// @description Move

if (global.pause) exit;

show_debug_message(depth);

event_inherited();

if (hp <= 0) exit;

anim_speed = 0.5;

switch(state) {
	case "IDLE": {
		image_speed = 0;
		sprite_index = sprite_walk;
		image_index = 0;
		vx = 0;
		vy = 0;
		break;
	}
	case "MOVING": {
		x += vx;
		y += vy;
		if (x == new_x && y == new_y) state = "IDLE";
		break;
	}
	case "LEFT": {
		image_speed = anim_speed*2;
		sprite_walk = spr_wrench_side;
		sprite_index = sprite_walk;
		image_xscale = 1;
		vx = -player_speed;
		vy = 0;
		break;
	}
	case "RIGHT": {
		image_speed = anim_speed*2;
		sprite_walk = spr_wrench_side;
		sprite_index = sprite_walk;
		image_xscale = -1;
		vx = player_speed;
		vy = 0;
		break;
	}
	case "ATTACK": {
		if (!place_meeting(x, y, obj_solid)) {
			with(instance_create_depth(x, y, 0, obj_wrench_weapon)) {
				if (other.image_xscale == -1) {
					direction = 0;
				} else {
					direction = 180;
				}
				speed = 4;
			}
		}
		state = "IDLE";
	}
}



if (state != "MOVING" && state != "IDLE") {
	new_x = x + (32 * sign(vx));
	new_y = y + (32 * sign(vy));
	if (!place_meeting(new_x, new_y, obj_solid) && !place_meeting(new_x, new_y, obj_enemy)) state = "MOVING";
}

if (irandom(100) < 1 && state != "MOVING") state = choose("LEFT", "RIGHT", "ATTACK");
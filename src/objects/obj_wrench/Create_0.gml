/// @description Init

event_inherited();
hp = 50;
damage = 25;

state = "IDLE";

sprite_walk = spr_wrench_side;
player_speed = 2;

lay_id = layer_get_id("Tiles_Ground");
map_id = layer_tilemap_get_id(lay_id);
x_tile = 16;
if (x > 320) x_tile = 624;
tile_data = tilemap_get_at_pixel(map_id, x_tile, y);
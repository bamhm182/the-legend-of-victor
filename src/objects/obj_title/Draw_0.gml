/// @description Draw Title Screen

draw_set_valign(fa_middle);
draw_set_halign(fa_center);
draw_set_font(fnt_gb);
draw_set_color(colors("darkest"));
draw_text(320, 335, "PRESS I TO BEGIN");

draw_set_font(fnt_gb_small);
draw_text(320,350, "Type ':HELP' for Help");

draw_sprite(spr_title,0,320,160);

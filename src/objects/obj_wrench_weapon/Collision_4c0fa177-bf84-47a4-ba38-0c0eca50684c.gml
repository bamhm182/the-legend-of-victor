/// @description Hurt Victor

if (other.iframes == 0) {
	if (global.sfx) audio_play_sound(snd_wrench_hit,10,false);
	other.hp -= damage;
	other.iframes = 30;
}
instance_destroy();
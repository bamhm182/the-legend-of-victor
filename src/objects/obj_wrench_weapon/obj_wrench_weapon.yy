{
    "id": "d7f87fd1-66da-45ed-843a-bb53a89426b7",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_wrench_weapon",
    "eventList": [
        {
            "id": "2aa953cf-8aab-444d-8bfd-c02a44fb0e81",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "d7f87fd1-66da-45ed-843a-bb53a89426b7"
        },
        {
            "id": "4c0fa177-bf84-47a4-ba38-0c0eca50684c",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "6cc53553-4744-4f81-b714-1cac90670a82",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "d7f87fd1-66da-45ed-843a-bb53a89426b7"
        },
        {
            "id": "a16da013-3350-4944-9c7e-2365f16eb92d",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "d7f87fd1-66da-45ed-843a-bb53a89426b7"
        },
        {
            "id": "e2536d70-f55d-44d7-9d13-3fa249b80cd9",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "f37b8281-b0c6-45b0-99c4-4846899fa0ab",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "d7f87fd1-66da-45ed-843a-bb53a89426b7"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "36866625-5480-47e6-9f96-795798dd57c5",
    "visible": true
}
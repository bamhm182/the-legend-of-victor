/// @description Rotate

if (global.pause) {
	speed = 0;
	exit;
} else {
	speed = 4;
}

if (direction == 0) {
	image_angle-=5;
} else {
	image_angle+=5;
}

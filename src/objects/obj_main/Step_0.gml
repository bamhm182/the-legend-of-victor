/// @description Process State

state_process(st_title);

if (global.sfx) {
	if (!audio_is_playing(snd_music_loop) && !audio_is_playing(snd_music_intro) && intro_played) {
		audio_play_sound(snd_music_loop,10,true);
	}
	if (!audio_is_playing(snd_music_intro) && !audio_is_playing(snd_music_loop) && !intro_played) {
		audio_play_sound(snd_music_intro,10,false);
		intro_played = true;
	}
}
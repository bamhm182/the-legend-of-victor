/// @description Init

state_init();

if (os_browser == browser_not_a_browser) {
	if (display_get_width() > 1920) {
		scale = 3;
		window_set_size(window_get_width() * scale, window_get_height() * scale);
	}
	window_set_position(200,200);
}

global.pause = false;
intro_played = false;
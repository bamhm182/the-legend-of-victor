/// @description Check Alive

if (hp <= 0) {
	sprite_index = spr_smoke;
	image_speed = 1;
	
	if (image_index > image_number - 1) {
		instance_destroy();
		global.curr_score++;
	}
}


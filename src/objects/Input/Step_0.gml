/// @description Key Presses

left = keyboard_check(ord("H")) || keyboard_check(vk_left);
right = keyboard_check(ord("L")) || keyboard_check(vk_right);
up = keyboard_check(ord("K")) || keyboard_check(vk_up);
down = keyboard_check(ord("J")) || keyboard_check(vk_down);

k_i = keyboard_check_pressed(ord("I"));
k_x = keyboard_check_pressed(ord("X"));
k_d = keyboard_check_pressed(ord("D"));
command = keyboard_check_pressed(186) && keyboard_check(vk_shift); // 186 is semicolon
enter = keyboard_check_pressed(vk_enter);
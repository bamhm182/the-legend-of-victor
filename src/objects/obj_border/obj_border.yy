{
    "id": "d3371979-3415-4c5c-9c7c-7abc9c0feae4",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_border",
    "eventList": [
        
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "f37b8281-b0c6-45b0-99c4-4846899fa0ab",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "c7a5280c-5549-4b39-b7d1-b7ac7be5459a",
    "visible": false
}
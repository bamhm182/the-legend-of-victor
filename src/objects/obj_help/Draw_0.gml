/// @description Draw Text

draw_set_color(colors("darkest"));
draw_rectangle(0,0,640,384, false);

_x = 320;
_y = 128;
draw_set_valign(fa_middle);
draw_set_halign(fa_center);
draw_set_font(fnt_gb);
draw_set_color(colors("lightest"));
draw_text(_x, _y, "HELP");

draw_text(_x, _y+96, "CONTROLS");


draw_set_halign(fa_right);
draw_text(_x*.75, _y+128,  "LEFT:  H");
draw_text(_x*.75, _y+144, "RIGHT:  L");
draw_text(_x*.75, _y+160, "UP:  K");
draw_text(_x*.75, _y+176, "DOWN:  J");
draw_text(_x*.75, _y+192, "RIGHT ATTACK:  X");
draw_text(_x*.75, _y+208, "JUMP ATTACK: DD");


draw_text(_x*1.85, _y+128, "COMMAND MODE:  :           ");
draw_text(_x*1.85, _y+144,     "TELEPORT:  :#LINE# ");
draw_text(_x*1.85, _y+160, "TOGGLE SOUND:  :SOUND ");
draw_text(_x*1.85, _y+176, "HELP MENU:  :HELP    ");
draw_text(_x*1.85, _y+192, "QUIT/BACK:   :Q          ");
{
    "id": "0dfab72e-39b3-494c-9eb8-f8e9d5f84722",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_help",
    "eventList": [
        {
            "id": "483d4aa3-7c06-4b94-9264-4bdcbcadc9e3",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "0dfab72e-39b3-494c-9eb8-f8e9d5f84722"
        },
        {
            "id": "a1e9048c-0763-4de5-94b6-5425b27ed2c1",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "0dfab72e-39b3-494c-9eb8-f8e9d5f84722"
        },
        {
            "id": "48d6b2b7-6df0-4063-80ed-83564bccf2bd",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "0dfab72e-39b3-494c-9eb8-f8e9d5f84722"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}
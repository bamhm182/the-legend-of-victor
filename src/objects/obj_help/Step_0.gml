/// @description Destroy if Unpaused

if (!global.pause) {
	instance_destroy();
}
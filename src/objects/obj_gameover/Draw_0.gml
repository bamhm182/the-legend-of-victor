/// @description Draw Game Over Screen

draw_self();
draw_set_valign(fa_middle);
draw_set_halign(fa_center);
draw_set_font(fnt_gb);
draw_set_color(colors("lightest"));
draw_text(320, 112, "GAME OVER");
draw_text(320, 176, "YOU " + verb);
draw_text(320, 192, global.curr_score);
draw_text(320, 208, descriptor + " WRENCH " + noun);
draw_text(320, 272, "Type ':Q' to Quit");
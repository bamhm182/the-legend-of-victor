{
    "id": "3bc4ba2c-0ee4-429a-9421-91a4307a1d30",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_gameover",
    "eventList": [
        {
            "id": "d75e4442-58b3-4acb-a851-d9353b571124",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "3bc4ba2c-0ee4-429a-9421-91a4307a1d30"
        },
        {
            "id": "4387d504-84a9-4254-add5-3f9bbd1214fd",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "3bc4ba2c-0ee4-429a-9421-91a4307a1d30"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "96990f9f-6831-4bd4-926c-166e541ead16",
    "visible": true
}
/// @description Init

//global.pause = true;

verb = choose("MURDERED", "SLAYED", "KILLED", "SNUFFED OUT", "DESTROYED", "INCINERATED", "BURNT", "IGNITED", "CREMATED");
descriptor = choose("SILLY", "EVIL", "INNOCENT", "FUN-LOVING", "OBNOXIOUS", "RIDICULOUS", "BIZZARE", "WICKED", "GOOFY", "HATE-FILLED", "FOOLISH");
noun = choose("MONSTERS", "BEASTS", "CREATURES", "PUPPIES", "FRIENDS", "ANIMALS", "CRITTERS");
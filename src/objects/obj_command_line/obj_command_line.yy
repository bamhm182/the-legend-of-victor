{
    "id": "b9c2bf3b-14cb-43ea-9149-10a3bf1de0ae",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_command_line",
    "eventList": [
        {
            "id": "b24efdcb-3d54-45d9-800e-7ba6c6056382",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "b9c2bf3b-14cb-43ea-9149-10a3bf1de0ae"
        },
        {
            "id": "aff392b3-1e8e-4dcc-85e5-fa93827ee54a",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "b9c2bf3b-14cb-43ea-9149-10a3bf1de0ae"
        },
        {
            "id": "902db5e1-805a-4dea-9a2d-51677dd28f30",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "b9c2bf3b-14cb-43ea-9149-10a3bf1de0ae"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "3d32412b-0677-4790-863e-1a27ed649a37",
    "visible": true
}
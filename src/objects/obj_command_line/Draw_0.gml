/// @description Draw Text

draw_self();
draw_set_valign(fa_middle);
draw_set_halign(fa_left);
draw_set_font(fnt_gb);
draw_set_color(colors("lightest"));
draw_text(x+16, y+16, command);
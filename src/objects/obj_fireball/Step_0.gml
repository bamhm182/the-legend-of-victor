/// @description Destroy at End

if (image_index > image_number - 1) {
	instance_destroy();
} else {
	damage = 50 / instance_number(obj_fireball);
}


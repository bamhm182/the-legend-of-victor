{
    "id": "409bf0a5-1a90-46c1-9a4a-d10250b39d97",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_fireball",
    "eventList": [
        {
            "id": "44637c2e-bb3c-46e8-bb7c-68e1c9683955",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "409bf0a5-1a90-46c1-9a4a-d10250b39d97"
        },
        {
            "id": "6deaf276-4b84-40be-8eaa-be7b2e4d7b9f",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "409bf0a5-1a90-46c1-9a4a-d10250b39d97"
        },
        {
            "id": "bcbf3499-efa7-4afd-8d79-500b489b1178",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "303fc5fb-60fe-4446-91de-ed41f2b8225c",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "409bf0a5-1a90-46c1-9a4a-d10250b39d97"
        },
        {
            "id": "6d302764-b7c7-4ee9-8117-c8e411ef1f4c",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "d7f87fd1-66da-45ed-843a-bb53a89426b7",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "409bf0a5-1a90-46c1-9a4a-d10250b39d97"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "c9955084-c0b9-4dd3-88a7-80f72f35f476",
    "visible": true
}
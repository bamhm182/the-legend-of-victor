{
    "id": "0a6a9230-9b3a-4216-8f04-d6bc46e4b933",
    "modelName": "GMFont",
    "mvc": "1.1",
    "name": "fnt_gb",
    "AntiAlias": 1,
    "TTFName": "${project_dir}\\fonts\\fnt_gb\\Early GameBoy.ttf",
    "ascenderOffset": 1,
    "bold": false,
    "charset": 0,
    "first": 0,
    "fontName": "Early GameBoy",
    "glyphOperations": 0,
    "glyphs": [
        {
            "Key": 32,
            "Value": {
                "id": "0db106f1-9695-469a-aec2-e7152f062a87",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 32,
                "h": 21,
                "offset": 0,
                "shift": 8,
                "w": 8,
                "x": 2,
                "y": 2
            }
        },
        {
            "Key": 33,
            "Value": {
                "id": "6d3418b7-c7ec-48b0-8bed-f88f439d6d34",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 33,
                "h": 21,
                "offset": 4,
                "shift": 16,
                "w": 6,
                "x": 215,
                "y": 71
            }
        },
        {
            "Key": 34,
            "Value": {
                "id": "8f280457-5128-439c-ae38-50260b38ad54",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 34,
                "h": 21,
                "offset": 0,
                "shift": 16,
                "w": 10,
                "x": 203,
                "y": 71
            }
        },
        {
            "Key": 35,
            "Value": {
                "id": "f77df431-8b70-471a-90f1-f117c09766ff",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 35,
                "h": 21,
                "offset": 0,
                "shift": 10,
                "w": 10,
                "x": 191,
                "y": 71
            }
        },
        {
            "Key": 36,
            "Value": {
                "id": "56d4db94-dfc0-400d-a4c7-13613f49f828",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 36,
                "h": 21,
                "offset": 1,
                "shift": 10,
                "w": 8,
                "x": 181,
                "y": 71
            }
        },
        {
            "Key": 37,
            "Value": {
                "id": "ce188ff7-b2f0-44b4-8bd5-a7e7e7f16af6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 37,
                "h": 21,
                "offset": 0,
                "shift": 10,
                "w": 10,
                "x": 169,
                "y": 71
            }
        },
        {
            "Key": 38,
            "Value": {
                "id": "785a4a7f-31bc-4c0d-8321-71a82d2d15c4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 38,
                "h": 21,
                "offset": 0,
                "shift": 10,
                "w": 10,
                "x": 157,
                "y": 71
            }
        },
        {
            "Key": 39,
            "Value": {
                "id": "7c6ba125-81bc-455b-af5b-0e283b4fbca9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 39,
                "h": 21,
                "offset": 4,
                "shift": 16,
                "w": 4,
                "x": 151,
                "y": 71
            }
        },
        {
            "Key": 40,
            "Value": {
                "id": "151155b1-931c-4ce9-b811-a2d12d7a6d3c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 40,
                "h": 21,
                "offset": -2,
                "shift": 16,
                "w": 14,
                "x": 135,
                "y": 71
            }
        },
        {
            "Key": 41,
            "Value": {
                "id": "7dc245dd-c6af-416e-920d-a14ee3470727",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 41,
                "h": 21,
                "offset": 0,
                "shift": 16,
                "w": 10,
                "x": 123,
                "y": 71
            }
        },
        {
            "Key": 42,
            "Value": {
                "id": "f1d4f5b2-1211-4bff-a644-3cd99e013eb6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 42,
                "h": 21,
                "offset": 0,
                "shift": 16,
                "w": 10,
                "x": 223,
                "y": 71
            }
        },
        {
            "Key": 43,
            "Value": {
                "id": "3a6a1c0e-6706-4452-b401-39e5a6630059",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 43,
                "h": 21,
                "offset": 1,
                "shift": 10,
                "w": 8,
                "x": 113,
                "y": 71
            }
        },
        {
            "Key": 44,
            "Value": {
                "id": "10b09d21-7c46-4c54-a575-538d4261cf10",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 44,
                "h": 21,
                "offset": 2,
                "shift": 16,
                "w": 4,
                "x": 93,
                "y": 71
            }
        },
        {
            "Key": 45,
            "Value": {
                "id": "ff20ac7a-c4a0-45c4-becd-0ed2e58951f7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 45,
                "h": 21,
                "offset": 2,
                "shift": 16,
                "w": 8,
                "x": 83,
                "y": 71
            }
        },
        {
            "Key": 46,
            "Value": {
                "id": "8f18c3e2-dae0-4aef-894b-6807e1986933",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 46,
                "h": 21,
                "offset": 0,
                "shift": 16,
                "w": 4,
                "x": 77,
                "y": 71
            }
        },
        {
            "Key": 47,
            "Value": {
                "id": "c751d458-bfe1-4ba6-8b8f-f469c2978fca",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 47,
                "h": 21,
                "offset": 1,
                "shift": 10,
                "w": 7,
                "x": 68,
                "y": 71
            }
        },
        {
            "Key": 48,
            "Value": {
                "id": "fb3d4c01-d3d4-4608-bcf7-95f14eedf572",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 48,
                "h": 21,
                "offset": 0,
                "shift": 16,
                "w": 12,
                "x": 54,
                "y": 71
            }
        },
        {
            "Key": 49,
            "Value": {
                "id": "f0d10a6e-a520-413e-b30d-ccca6d1db473",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 49,
                "h": 21,
                "offset": 2,
                "shift": 16,
                "w": 8,
                "x": 44,
                "y": 71
            }
        },
        {
            "Key": 50,
            "Value": {
                "id": "3d72f5a4-fd2f-4ca1-b820-ee50f13195a7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 50,
                "h": 21,
                "offset": 0,
                "shift": 16,
                "w": 12,
                "x": 30,
                "y": 71
            }
        },
        {
            "Key": 51,
            "Value": {
                "id": "1e36619b-6f71-4994-b16f-fe25a39c5d76",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 51,
                "h": 21,
                "offset": 0,
                "shift": 16,
                "w": 12,
                "x": 16,
                "y": 71
            }
        },
        {
            "Key": 52,
            "Value": {
                "id": "9f78baa1-a0ff-43c0-91cd-af5b41c33c8b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 52,
                "h": 21,
                "offset": 0,
                "shift": 16,
                "w": 12,
                "x": 2,
                "y": 71
            }
        },
        {
            "Key": 53,
            "Value": {
                "id": "3d081650-4b45-4ab3-a5d0-844e67b52a1f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 53,
                "h": 21,
                "offset": 0,
                "shift": 16,
                "w": 12,
                "x": 99,
                "y": 71
            }
        },
        {
            "Key": 54,
            "Value": {
                "id": "951094e1-9cbf-4047-b0ce-02a24c7d7ce0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 54,
                "h": 21,
                "offset": 0,
                "shift": 16,
                "w": 12,
                "x": 235,
                "y": 71
            }
        },
        {
            "Key": 55,
            "Value": {
                "id": "8d67bd89-b349-4850-98f0-e76f20d59bc9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 55,
                "h": 21,
                "offset": 0,
                "shift": 16,
                "w": 12,
                "x": 2,
                "y": 94
            }
        },
        {
            "Key": 56,
            "Value": {
                "id": "cf64f5fd-e351-423a-b364-a3036abbd66c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 56,
                "h": 21,
                "offset": 0,
                "shift": 16,
                "w": 12,
                "x": 16,
                "y": 94
            }
        },
        {
            "Key": 57,
            "Value": {
                "id": "532369c7-6954-4c93-8f78-c7fb2441f4e0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 57,
                "h": 21,
                "offset": 0,
                "shift": 16,
                "w": 12,
                "x": 40,
                "y": 117
            }
        },
        {
            "Key": 58,
            "Value": {
                "id": "7140c3c4-4dbf-4942-84cd-a7380446758d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 58,
                "h": 21,
                "offset": 2,
                "shift": 16,
                "w": 4,
                "x": 34,
                "y": 117
            }
        },
        {
            "Key": 59,
            "Value": {
                "id": "7ddfe320-ce83-49b0-bf0c-dc535d8203b8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 59,
                "h": 21,
                "offset": 2,
                "shift": 16,
                "w": 4,
                "x": 28,
                "y": 117
            }
        },
        {
            "Key": 60,
            "Value": {
                "id": "702a3860-729d-4b4d-9687-8b10f3b73435",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 60,
                "h": 21,
                "offset": 0,
                "shift": 18,
                "w": 14,
                "x": 12,
                "y": 117
            }
        },
        {
            "Key": 61,
            "Value": {
                "id": "a3900a44-8f97-43d4-9f72-3d33a71da04a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 61,
                "h": 21,
                "offset": 1,
                "shift": 10,
                "w": 8,
                "x": 2,
                "y": 117
            }
        },
        {
            "Key": 62,
            "Value": {
                "id": "7db2cda4-241d-4837-8405-e6128c6f3854",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 62,
                "h": 21,
                "offset": 2,
                "shift": 18,
                "w": 10,
                "x": 234,
                "y": 94
            }
        },
        {
            "Key": 63,
            "Value": {
                "id": "2156d3f9-c5f0-48a2-bbb3-c4392e6dcdc2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 63,
                "h": 21,
                "offset": -2,
                "shift": 16,
                "w": 12,
                "x": 220,
                "y": 94
            }
        },
        {
            "Key": 64,
            "Value": {
                "id": "e8868747-9861-464c-8fa7-b37e5ffc0314",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 64,
                "h": 21,
                "offset": 0,
                "shift": 10,
                "w": 10,
                "x": 208,
                "y": 94
            }
        },
        {
            "Key": 65,
            "Value": {
                "id": "4ec9f1ba-cb0e-499c-a6ea-491e6ebbb2c7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 65,
                "h": 21,
                "offset": 0,
                "shift": 16,
                "w": 12,
                "x": 194,
                "y": 94
            }
        },
        {
            "Key": 66,
            "Value": {
                "id": "55392283-9f42-4674-8b60-ffdeaf65bd6b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 66,
                "h": 21,
                "offset": 0,
                "shift": 16,
                "w": 12,
                "x": 180,
                "y": 94
            }
        },
        {
            "Key": 67,
            "Value": {
                "id": "5027d376-0eda-4e46-a6bd-9b4b2203505f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 67,
                "h": 21,
                "offset": 0,
                "shift": 16,
                "w": 12,
                "x": 166,
                "y": 94
            }
        },
        {
            "Key": 68,
            "Value": {
                "id": "63d46b55-79f4-4f64-9f99-2a390975ff0f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 68,
                "h": 21,
                "offset": 0,
                "shift": 16,
                "w": 12,
                "x": 152,
                "y": 94
            }
        },
        {
            "Key": 69,
            "Value": {
                "id": "8fbb5c74-61ed-40af-aca7-133e9a8f9156",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 69,
                "h": 21,
                "offset": 0,
                "shift": 16,
                "w": 12,
                "x": 138,
                "y": 94
            }
        },
        {
            "Key": 70,
            "Value": {
                "id": "ce406933-f53b-4f44-beec-5f154e627400",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 70,
                "h": 21,
                "offset": 0,
                "shift": 16,
                "w": 12,
                "x": 124,
                "y": 94
            }
        },
        {
            "Key": 71,
            "Value": {
                "id": "43b56f1f-9d87-4fe3-8e8f-83ce0e247208",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 71,
                "h": 21,
                "offset": 0,
                "shift": 16,
                "w": 12,
                "x": 110,
                "y": 94
            }
        },
        {
            "Key": 72,
            "Value": {
                "id": "84ce962b-a4be-4bde-823e-5dac467b0709",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 72,
                "h": 21,
                "offset": 0,
                "shift": 16,
                "w": 12,
                "x": 96,
                "y": 94
            }
        },
        {
            "Key": 73,
            "Value": {
                "id": "77ca4cc6-9462-4851-82e7-25ca251bbd35",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 73,
                "h": 21,
                "offset": 2,
                "shift": 16,
                "w": 8,
                "x": 86,
                "y": 94
            }
        },
        {
            "Key": 74,
            "Value": {
                "id": "766a2f29-6b3a-4d72-bf75-51e4806d7940",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 74,
                "h": 21,
                "offset": 0,
                "shift": 16,
                "w": 12,
                "x": 72,
                "y": 94
            }
        },
        {
            "Key": 75,
            "Value": {
                "id": "f4c5d40e-bd6a-4a16-8dd0-53fe78609d42",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 75,
                "h": 21,
                "offset": 0,
                "shift": 16,
                "w": 12,
                "x": 58,
                "y": 94
            }
        },
        {
            "Key": 76,
            "Value": {
                "id": "13e4ced3-a9d2-42bb-929f-8baa58676428",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 76,
                "h": 21,
                "offset": 0,
                "shift": 16,
                "w": 12,
                "x": 44,
                "y": 94
            }
        },
        {
            "Key": 77,
            "Value": {
                "id": "61e2fdf9-279f-4fe8-a7bd-60dc96e5a674",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 77,
                "h": 21,
                "offset": 0,
                "shift": 16,
                "w": 12,
                "x": 30,
                "y": 94
            }
        },
        {
            "Key": 78,
            "Value": {
                "id": "997fd467-78ee-488b-a6c2-6b69ad0f97d6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 78,
                "h": 21,
                "offset": 0,
                "shift": 16,
                "w": 12,
                "x": 240,
                "y": 48
            }
        },
        {
            "Key": 79,
            "Value": {
                "id": "d8c0bf0f-e965-4edd-adf2-1bdca74cd718",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 79,
                "h": 21,
                "offset": 0,
                "shift": 16,
                "w": 12,
                "x": 226,
                "y": 48
            }
        },
        {
            "Key": 80,
            "Value": {
                "id": "db8b3b94-2407-47db-9bbe-cb017b2688c4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 80,
                "h": 21,
                "offset": 0,
                "shift": 16,
                "w": 12,
                "x": 212,
                "y": 48
            }
        },
        {
            "Key": 81,
            "Value": {
                "id": "04bf0262-b88c-43cd-aba0-608065f76b90",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 81,
                "h": 21,
                "offset": 0,
                "shift": 16,
                "w": 12,
                "x": 48,
                "y": 25
            }
        },
        {
            "Key": 82,
            "Value": {
                "id": "ffc7ae02-4550-4275-9220-cd81ccf2ccc7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 82,
                "h": 21,
                "offset": 0,
                "shift": 16,
                "w": 12,
                "x": 16,
                "y": 25
            }
        },
        {
            "Key": 83,
            "Value": {
                "id": "23c822c5-8ffe-467e-b563-03a063e956b6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 83,
                "h": 21,
                "offset": 0,
                "shift": 16,
                "w": 12,
                "x": 2,
                "y": 25
            }
        },
        {
            "Key": 84,
            "Value": {
                "id": "88a1ab3b-69df-4ada-a33f-a388b4fa7b52",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 84,
                "h": 21,
                "offset": 0,
                "shift": 16,
                "w": 12,
                "x": 233,
                "y": 2
            }
        },
        {
            "Key": 85,
            "Value": {
                "id": "fb0e7884-a858-4d9a-9926-8dfd0a341538",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 85,
                "h": 21,
                "offset": 0,
                "shift": 16,
                "w": 12,
                "x": 219,
                "y": 2
            }
        },
        {
            "Key": 86,
            "Value": {
                "id": "33f675d3-4bba-4825-bc8b-521300042218",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 86,
                "h": 21,
                "offset": 0,
                "shift": 16,
                "w": 12,
                "x": 205,
                "y": 2
            }
        },
        {
            "Key": 87,
            "Value": {
                "id": "bb783a31-6ea3-4c27-827a-c9750fd05647",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 87,
                "h": 21,
                "offset": 0,
                "shift": 16,
                "w": 12,
                "x": 191,
                "y": 2
            }
        },
        {
            "Key": 88,
            "Value": {
                "id": "889f5127-bd18-4871-9abb-18068702de17",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 88,
                "h": 21,
                "offset": 0,
                "shift": 16,
                "w": 12,
                "x": 177,
                "y": 2
            }
        },
        {
            "Key": 89,
            "Value": {
                "id": "7a09f2ea-19ec-46b5-8ec0-1ab262493eaf",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 89,
                "h": 21,
                "offset": 0,
                "shift": 16,
                "w": 12,
                "x": 163,
                "y": 2
            }
        },
        {
            "Key": 90,
            "Value": {
                "id": "557c68b4-11ba-4b8b-bf22-8607fdd60c69",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 90,
                "h": 21,
                "offset": 0,
                "shift": 16,
                "w": 12,
                "x": 149,
                "y": 2
            }
        },
        {
            "Key": 91,
            "Value": {
                "id": "708b666e-7934-4ec4-b907-ce8b4db577a0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 91,
                "h": 21,
                "offset": -2,
                "shift": 16,
                "w": 16,
                "x": 30,
                "y": 25
            }
        },
        {
            "Key": 92,
            "Value": {
                "id": "96e3b6d0-e66c-430c-9314-e214462aa7c5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 92,
                "h": 21,
                "offset": 1,
                "shift": 10,
                "w": 7,
                "x": 140,
                "y": 2
            }
        },
        {
            "Key": 93,
            "Value": {
                "id": "d82e0543-b030-412c-b63a-d7e90d735f25",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 93,
                "h": 21,
                "offset": 0,
                "shift": 16,
                "w": 12,
                "x": 112,
                "y": 2
            }
        },
        {
            "Key": 94,
            "Value": {
                "id": "875ef870-7e3c-4a4b-9bd3-96d6a04c0595",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 94,
                "h": 21,
                "offset": 0,
                "shift": 10,
                "w": 9,
                "x": 101,
                "y": 2
            }
        },
        {
            "Key": 95,
            "Value": {
                "id": "ca33d5b4-b0ca-4292-8f92-d2be441571b6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 95,
                "h": 21,
                "offset": -1,
                "shift": 10,
                "w": 11,
                "x": 88,
                "y": 2
            }
        },
        {
            "Key": 96,
            "Value": {
                "id": "7d68059c-8de6-47c3-9bb2-782d101a13b6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 96,
                "h": 21,
                "offset": 3,
                "shift": 10,
                "w": 4,
                "x": 82,
                "y": 2
            }
        },
        {
            "Key": 97,
            "Value": {
                "id": "78322ae4-f0c7-4761-a1c7-2d4dad97f90b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 97,
                "h": 21,
                "offset": 0,
                "shift": 16,
                "w": 12,
                "x": 68,
                "y": 2
            }
        },
        {
            "Key": 98,
            "Value": {
                "id": "b0643ea0-e4f8-457e-95df-f839f9625bc9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 98,
                "h": 21,
                "offset": 0,
                "shift": 16,
                "w": 12,
                "x": 54,
                "y": 2
            }
        },
        {
            "Key": 99,
            "Value": {
                "id": "3f7c8f4f-e0e8-40eb-83e5-1bce046d3b66",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 99,
                "h": 21,
                "offset": 0,
                "shift": 16,
                "w": 12,
                "x": 40,
                "y": 2
            }
        },
        {
            "Key": 100,
            "Value": {
                "id": "b2427e42-4a53-433c-afa7-59497032ae99",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 100,
                "h": 21,
                "offset": 0,
                "shift": 16,
                "w": 12,
                "x": 26,
                "y": 2
            }
        },
        {
            "Key": 101,
            "Value": {
                "id": "2e007abf-fd1b-4c03-8d83-387bea5a888a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 101,
                "h": 21,
                "offset": 0,
                "shift": 16,
                "w": 12,
                "x": 12,
                "y": 2
            }
        },
        {
            "Key": 102,
            "Value": {
                "id": "dd68fd13-479e-4317-8896-67999f980acf",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 102,
                "h": 21,
                "offset": 0,
                "shift": 16,
                "w": 12,
                "x": 126,
                "y": 2
            }
        },
        {
            "Key": 103,
            "Value": {
                "id": "d6f5ef6d-afb5-43ac-b8cb-ed02c8ecc766",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 103,
                "h": 21,
                "offset": 0,
                "shift": 16,
                "w": 12,
                "x": 62,
                "y": 25
            }
        },
        {
            "Key": 104,
            "Value": {
                "id": "2a2bf8ca-d7e6-438d-9caf-2e10bc8ba8d0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 104,
                "h": 21,
                "offset": 0,
                "shift": 16,
                "w": 12,
                "x": 44,
                "y": 48
            }
        },
        {
            "Key": 105,
            "Value": {
                "id": "c8f1e043-10fb-4227-9106-11baa5f76cb9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 105,
                "h": 21,
                "offset": 2,
                "shift": 16,
                "w": 8,
                "x": 76,
                "y": 25
            }
        },
        {
            "Key": 106,
            "Value": {
                "id": "f151b417-2eef-4409-89dc-32ed65087af8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 106,
                "h": 21,
                "offset": 0,
                "shift": 16,
                "w": 12,
                "x": 184,
                "y": 48
            }
        },
        {
            "Key": 107,
            "Value": {
                "id": "a40ea7b3-e227-499e-99ea-a88aaf3c2fd7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 107,
                "h": 21,
                "offset": 0,
                "shift": 16,
                "w": 12,
                "x": 170,
                "y": 48
            }
        },
        {
            "Key": 108,
            "Value": {
                "id": "2a139bf9-e2b4-47e0-8e29-86c56fe7be26",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 108,
                "h": 21,
                "offset": 0,
                "shift": 16,
                "w": 12,
                "x": 156,
                "y": 48
            }
        },
        {
            "Key": 109,
            "Value": {
                "id": "ea857b45-a3c1-4b5a-8e1c-e85b612b8e1f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 109,
                "h": 21,
                "offset": 0,
                "shift": 16,
                "w": 12,
                "x": 142,
                "y": 48
            }
        },
        {
            "Key": 110,
            "Value": {
                "id": "117b4c7e-1374-4690-931b-4d3484c09231",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 110,
                "h": 21,
                "offset": 0,
                "shift": 16,
                "w": 12,
                "x": 128,
                "y": 48
            }
        },
        {
            "Key": 111,
            "Value": {
                "id": "b854d070-4ab5-4fd3-b886-9f9f2df7394b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 111,
                "h": 21,
                "offset": 0,
                "shift": 16,
                "w": 12,
                "x": 114,
                "y": 48
            }
        },
        {
            "Key": 112,
            "Value": {
                "id": "3e79ff93-9d14-44a2-ac05-028a4bad724b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 112,
                "h": 21,
                "offset": 0,
                "shift": 16,
                "w": 12,
                "x": 100,
                "y": 48
            }
        },
        {
            "Key": 113,
            "Value": {
                "id": "1c23d151-dc45-4c36-84bf-941f7c5d634f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 113,
                "h": 21,
                "offset": 0,
                "shift": 16,
                "w": 12,
                "x": 86,
                "y": 48
            }
        },
        {
            "Key": 114,
            "Value": {
                "id": "42de8f86-4fdb-4cd6-89f3-d954bd54ecef",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 114,
                "h": 21,
                "offset": 0,
                "shift": 16,
                "w": 12,
                "x": 72,
                "y": 48
            }
        },
        {
            "Key": 115,
            "Value": {
                "id": "b32ced26-a82a-4736-950a-27194d5638f5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 115,
                "h": 21,
                "offset": 0,
                "shift": 16,
                "w": 12,
                "x": 198,
                "y": 48
            }
        },
        {
            "Key": 116,
            "Value": {
                "id": "2f7ae8a0-d314-484e-9d76-e24492015b51",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 116,
                "h": 21,
                "offset": 0,
                "shift": 16,
                "w": 12,
                "x": 58,
                "y": 48
            }
        },
        {
            "Key": 117,
            "Value": {
                "id": "0423c849-2c97-4af4-924a-117da3f869a9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 117,
                "h": 21,
                "offset": 0,
                "shift": 16,
                "w": 12,
                "x": 30,
                "y": 48
            }
        },
        {
            "Key": 118,
            "Value": {
                "id": "5e14dff3-39f7-4555-b79e-c110feb9a6a1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 118,
                "h": 21,
                "offset": 0,
                "shift": 16,
                "w": 12,
                "x": 16,
                "y": 48
            }
        },
        {
            "Key": 119,
            "Value": {
                "id": "31a4055e-d89b-45a3-94c0-ef8227dab0fd",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 119,
                "h": 21,
                "offset": 0,
                "shift": 16,
                "w": 12,
                "x": 2,
                "y": 48
            }
        },
        {
            "Key": 120,
            "Value": {
                "id": "e525e0d7-b9e6-479a-8364-3a4d1d7a473a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 120,
                "h": 21,
                "offset": 0,
                "shift": 16,
                "w": 12,
                "x": 234,
                "y": 25
            }
        },
        {
            "Key": 121,
            "Value": {
                "id": "910de4fa-f381-4432-9d8c-475866a5e398",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 121,
                "h": 21,
                "offset": 0,
                "shift": 16,
                "w": 12,
                "x": 220,
                "y": 25
            }
        },
        {
            "Key": 122,
            "Value": {
                "id": "b4eded9c-fdaf-42bb-a8d9-3b3120312fbe",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 122,
                "h": 21,
                "offset": 0,
                "shift": 16,
                "w": 12,
                "x": 206,
                "y": 25
            }
        },
        {
            "Key": 123,
            "Value": {
                "id": "b3fafac4-369b-4656-8439-a3703b9fd4f2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 123,
                "h": 21,
                "offset": 1,
                "shift": 10,
                "w": 7,
                "x": 197,
                "y": 25
            }
        },
        {
            "Key": 124,
            "Value": {
                "id": "a675c79a-d0ff-4324-98ca-11ea7ce04e8a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 124,
                "h": 21,
                "offset": 0,
                "shift": 100,
                "w": 100,
                "x": 95,
                "y": 25
            }
        },
        {
            "Key": 125,
            "Value": {
                "id": "e3ff0b8d-6c2c-48c7-bc6b-3edf5bb858fd",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 125,
                "h": 21,
                "offset": 1,
                "shift": 10,
                "w": 7,
                "x": 86,
                "y": 25
            }
        },
        {
            "Key": 126,
            "Value": {
                "id": "73637780-650d-4837-a797-d55689960558",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 126,
                "h": 21,
                "offset": 0,
                "shift": 16,
                "w": 8,
                "x": 54,
                "y": 117
            }
        },
        {
            "Key": 9647,
            "Value": {
                "id": "eaacf689-d619-4c54-816f-2b05ac0cdc60",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 9647,
                "h": 21,
                "offset": 3,
                "shift": 16,
                "w": 10,
                "x": 64,
                "y": 117
            }
        }
    ],
    "hinting": 0,
    "includeTTF": true,
    "interpreter": 0,
    "italic": false,
    "kerningPairs": [
        
    ],
    "last": 0,
    "maintainGms1Font": false,
    "pointRounding": 0,
    "ranges": [
        {
            "x": 32,
            "y": 127
        },
        {
            "x": 9647,
            "y": 9647
        }
    ],
    "sampleText": "abcdef ABCDEF\\u000a0123456789 .,<>\"'&!?\\u000athe quick brown fox jumps over the lazy dog\\u000aTHE QUICK BROWN FOX JUMPS OVER THE LAZY DOG\\u000aDefault character: ▯ (9647)",
    "size": 12,
    "styleName": "Regular",
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f"
}
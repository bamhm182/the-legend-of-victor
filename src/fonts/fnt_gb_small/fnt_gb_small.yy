{
    "id": "e4ffd73c-b60d-489d-8787-f634a65a499a",
    "modelName": "GMFont",
    "mvc": "1.1",
    "name": "fnt_gb_small",
    "AntiAlias": 1,
    "TTFName": "${project_dir}\\fonts\\fnt_gb_small\\Early GameBoy.ttf",
    "ascenderOffset": 0,
    "bold": false,
    "charset": 0,
    "first": 0,
    "fontName": "Early GameBoy",
    "glyphOperations": 0,
    "glyphs": [
        {
            "Key": 32,
            "Value": {
                "id": "82e4a18d-fbee-4820-85df-d0ea8b00e54d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 32,
                "h": 10,
                "offset": 0,
                "shift": 4,
                "w": 4,
                "x": 2,
                "y": 2
            }
        },
        {
            "Key": 33,
            "Value": {
                "id": "4dda8126-0de0-43d8-8e12-d972042f7849",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 33,
                "h": 10,
                "offset": 2,
                "shift": 8,
                "w": 3,
                "x": 114,
                "y": 50
            }
        },
        {
            "Key": 34,
            "Value": {
                "id": "0e590c5d-6a50-4ec1-aeef-37d918f3a400",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 34,
                "h": 10,
                "offset": 0,
                "shift": 8,
                "w": 5,
                "x": 107,
                "y": 50
            }
        },
        {
            "Key": 35,
            "Value": {
                "id": "1f419016-41e1-4d37-957d-52ad183de6ba",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 35,
                "h": 10,
                "offset": 0,
                "shift": 5,
                "w": 5,
                "x": 100,
                "y": 50
            }
        },
        {
            "Key": 36,
            "Value": {
                "id": "174d05f2-a5db-48cd-9444-64890002d190",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 36,
                "h": 10,
                "offset": 0,
                "shift": 5,
                "w": 5,
                "x": 93,
                "y": 50
            }
        },
        {
            "Key": 37,
            "Value": {
                "id": "b5c5454e-1d45-432c-a23d-a3ca2f5afd41",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 37,
                "h": 10,
                "offset": 0,
                "shift": 5,
                "w": 5,
                "x": 86,
                "y": 50
            }
        },
        {
            "Key": 38,
            "Value": {
                "id": "c9877d3d-119c-4239-a624-c18afb2ae7fa",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 38,
                "h": 10,
                "offset": 0,
                "shift": 5,
                "w": 5,
                "x": 79,
                "y": 50
            }
        },
        {
            "Key": 39,
            "Value": {
                "id": "45640ada-3b90-44cd-9431-144aa0151a66",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 39,
                "h": 10,
                "offset": 2,
                "shift": 8,
                "w": 2,
                "x": 75,
                "y": 50
            }
        },
        {
            "Key": 40,
            "Value": {
                "id": "027c325e-eef6-455e-8d04-e86062d041f1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 40,
                "h": 10,
                "offset": -1,
                "shift": 8,
                "w": 7,
                "x": 66,
                "y": 50
            }
        },
        {
            "Key": 41,
            "Value": {
                "id": "2a244f54-3295-4bf7-9c97-ab80387aae01",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 41,
                "h": 10,
                "offset": 0,
                "shift": 8,
                "w": 5,
                "x": 59,
                "y": 50
            }
        },
        {
            "Key": 42,
            "Value": {
                "id": "d0226fd7-e19d-4f11-b4e9-308a94c0cf16",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 42,
                "h": 10,
                "offset": 0,
                "shift": 8,
                "w": 5,
                "x": 119,
                "y": 50
            }
        },
        {
            "Key": 43,
            "Value": {
                "id": "f51171e4-ac90-4b95-b2fe-d78d6efb76eb",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 43,
                "h": 10,
                "offset": 0,
                "shift": 5,
                "w": 5,
                "x": 52,
                "y": 50
            }
        },
        {
            "Key": 44,
            "Value": {
                "id": "bc05a00f-ab2e-42c3-b572-cb7b82025759",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 44,
                "h": 10,
                "offset": 1,
                "shift": 8,
                "w": 2,
                "x": 40,
                "y": 50
            }
        },
        {
            "Key": 45,
            "Value": {
                "id": "48195cb9-3ef6-4ecb-bb1b-0e8f944c6754",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 45,
                "h": 10,
                "offset": 1,
                "shift": 8,
                "w": 4,
                "x": 34,
                "y": 50
            }
        },
        {
            "Key": 46,
            "Value": {
                "id": "478f7b08-5350-418b-ad62-ee2ad82c7e98",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 46,
                "h": 10,
                "offset": 0,
                "shift": 8,
                "w": 2,
                "x": 30,
                "y": 50
            }
        },
        {
            "Key": 47,
            "Value": {
                "id": "706d5095-46e3-4230-8cac-b61a2a3774b3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 47,
                "h": 10,
                "offset": 0,
                "shift": 5,
                "w": 4,
                "x": 24,
                "y": 50
            }
        },
        {
            "Key": 48,
            "Value": {
                "id": "ee85c75b-b5cb-48d4-9f96-f3936e2caff2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 48,
                "h": 10,
                "offset": 0,
                "shift": 8,
                "w": 6,
                "x": 16,
                "y": 50
            }
        },
        {
            "Key": 49,
            "Value": {
                "id": "0a9bc83f-c087-455a-80da-b6c811f5975c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 49,
                "h": 10,
                "offset": 1,
                "shift": 8,
                "w": 4,
                "x": 10,
                "y": 50
            }
        },
        {
            "Key": 50,
            "Value": {
                "id": "21b3e325-b268-461b-af15-cbb6da16c397",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 50,
                "h": 10,
                "offset": 0,
                "shift": 8,
                "w": 6,
                "x": 2,
                "y": 50
            }
        },
        {
            "Key": 51,
            "Value": {
                "id": "d647936e-b18b-41d8-8348-6a982fb85858",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 51,
                "h": 10,
                "offset": 0,
                "shift": 8,
                "w": 6,
                "x": 114,
                "y": 38
            }
        },
        {
            "Key": 52,
            "Value": {
                "id": "6b80799d-849a-4401-a1d1-58636a5cbdba",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 52,
                "h": 10,
                "offset": 0,
                "shift": 8,
                "w": 6,
                "x": 106,
                "y": 38
            }
        },
        {
            "Key": 53,
            "Value": {
                "id": "77fb03ee-c7ee-4afa-afa6-e68ae5ca0eca",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 53,
                "h": 10,
                "offset": 0,
                "shift": 8,
                "w": 6,
                "x": 44,
                "y": 50
            }
        },
        {
            "Key": 54,
            "Value": {
                "id": "7c09babd-9eac-4054-9186-6d93d7c2421b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 54,
                "h": 10,
                "offset": 0,
                "shift": 8,
                "w": 6,
                "x": 2,
                "y": 62
            }
        },
        {
            "Key": 55,
            "Value": {
                "id": "90739d6a-61e0-4758-8b47-a34aa8a5def9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 55,
                "h": 10,
                "offset": 0,
                "shift": 8,
                "w": 6,
                "x": 10,
                "y": 62
            }
        },
        {
            "Key": 56,
            "Value": {
                "id": "51f0b717-4906-48f9-b01e-71d08039bbca",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 56,
                "h": 10,
                "offset": 0,
                "shift": 8,
                "w": 6,
                "x": 18,
                "y": 62
            }
        },
        {
            "Key": 57,
            "Value": {
                "id": "0b906c57-6699-4178-9e75-240c6d857926",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 57,
                "h": 10,
                "offset": 0,
                "shift": 8,
                "w": 6,
                "x": 56,
                "y": 74
            }
        },
        {
            "Key": 58,
            "Value": {
                "id": "336eea3e-20ad-40ef-bb0c-4370dfe2116e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 58,
                "h": 10,
                "offset": 1,
                "shift": 8,
                "w": 2,
                "x": 52,
                "y": 74
            }
        },
        {
            "Key": 59,
            "Value": {
                "id": "8bc637de-6c6f-4593-962c-f43adb1c2da3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 59,
                "h": 10,
                "offset": 1,
                "shift": 8,
                "w": 2,
                "x": 48,
                "y": 74
            }
        },
        {
            "Key": 60,
            "Value": {
                "id": "689f2f21-dc6a-48cf-b2dc-19ebe370d80d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 60,
                "h": 10,
                "offset": 0,
                "shift": 9,
                "w": 7,
                "x": 39,
                "y": 74
            }
        },
        {
            "Key": 61,
            "Value": {
                "id": "578b2163-36ff-4194-b13c-ebf267fc8ff0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 61,
                "h": 10,
                "offset": 0,
                "shift": 5,
                "w": 5,
                "x": 32,
                "y": 74
            }
        },
        {
            "Key": 62,
            "Value": {
                "id": "f3030707-680c-4b25-8013-5b6a1be93154",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 62,
                "h": 10,
                "offset": 1,
                "shift": 9,
                "w": 5,
                "x": 25,
                "y": 74
            }
        },
        {
            "Key": 63,
            "Value": {
                "id": "11b7a760-cf4e-4695-b9a6-37aa5bb0f7a4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 63,
                "h": 10,
                "offset": -1,
                "shift": 8,
                "w": 6,
                "x": 17,
                "y": 74
            }
        },
        {
            "Key": 64,
            "Value": {
                "id": "8611cbeb-35be-4a6c-95f4-38ece679a483",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 64,
                "h": 10,
                "offset": 0,
                "shift": 5,
                "w": 5,
                "x": 10,
                "y": 74
            }
        },
        {
            "Key": 65,
            "Value": {
                "id": "e527b024-b378-4952-aada-c6fb1042f42e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 65,
                "h": 10,
                "offset": 0,
                "shift": 8,
                "w": 6,
                "x": 2,
                "y": 74
            }
        },
        {
            "Key": 66,
            "Value": {
                "id": "63639e56-3ed1-4e0b-af0e-c51f9ceab3de",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 66,
                "h": 10,
                "offset": 0,
                "shift": 8,
                "w": 6,
                "x": 112,
                "y": 62
            }
        },
        {
            "Key": 67,
            "Value": {
                "id": "80956692-a16b-4505-bf46-4c1e03339e8d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 67,
                "h": 10,
                "offset": 0,
                "shift": 8,
                "w": 6,
                "x": 104,
                "y": 62
            }
        },
        {
            "Key": 68,
            "Value": {
                "id": "2d333c9c-15ac-42d1-97bb-75a59fb4a092",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 68,
                "h": 10,
                "offset": 0,
                "shift": 8,
                "w": 6,
                "x": 96,
                "y": 62
            }
        },
        {
            "Key": 69,
            "Value": {
                "id": "3f0527c5-2e25-443c-bc9b-9f36626454bd",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 69,
                "h": 10,
                "offset": 0,
                "shift": 8,
                "w": 6,
                "x": 88,
                "y": 62
            }
        },
        {
            "Key": 70,
            "Value": {
                "id": "78d74ccc-80b9-4791-b54c-6e65ed705258",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 70,
                "h": 10,
                "offset": 0,
                "shift": 8,
                "w": 6,
                "x": 80,
                "y": 62
            }
        },
        {
            "Key": 71,
            "Value": {
                "id": "deabdee5-2247-4a95-adf7-ded03be48fbb",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 71,
                "h": 10,
                "offset": 0,
                "shift": 8,
                "w": 6,
                "x": 72,
                "y": 62
            }
        },
        {
            "Key": 72,
            "Value": {
                "id": "6c09621a-9d72-42ae-b591-c1e05579c06c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 72,
                "h": 10,
                "offset": 0,
                "shift": 8,
                "w": 6,
                "x": 64,
                "y": 62
            }
        },
        {
            "Key": 73,
            "Value": {
                "id": "6b50be2b-ce46-4bd1-b353-d36be095028f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 73,
                "h": 10,
                "offset": 1,
                "shift": 8,
                "w": 4,
                "x": 58,
                "y": 62
            }
        },
        {
            "Key": 74,
            "Value": {
                "id": "d4e877e1-70f9-4283-8e15-3596ae38b0a4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 74,
                "h": 10,
                "offset": 0,
                "shift": 8,
                "w": 6,
                "x": 50,
                "y": 62
            }
        },
        {
            "Key": 75,
            "Value": {
                "id": "67cdc5ae-5375-4207-bee0-aa3d24f7606f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 75,
                "h": 10,
                "offset": 0,
                "shift": 8,
                "w": 6,
                "x": 42,
                "y": 62
            }
        },
        {
            "Key": 76,
            "Value": {
                "id": "21a4f84d-67fa-4100-a757-f02965a70444",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 76,
                "h": 10,
                "offset": 0,
                "shift": 8,
                "w": 6,
                "x": 34,
                "y": 62
            }
        },
        {
            "Key": 77,
            "Value": {
                "id": "261177a7-1823-4d6d-8e31-bbe16d841915",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 77,
                "h": 10,
                "offset": 0,
                "shift": 8,
                "w": 6,
                "x": 26,
                "y": 62
            }
        },
        {
            "Key": 78,
            "Value": {
                "id": "cccdec94-4d6f-4440-97c8-cd06a3875d9c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 78,
                "h": 10,
                "offset": 0,
                "shift": 8,
                "w": 6,
                "x": 98,
                "y": 38
            }
        },
        {
            "Key": 79,
            "Value": {
                "id": "09bbcb14-b6f5-428d-b278-90c0ad929e75",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 79,
                "h": 10,
                "offset": 0,
                "shift": 8,
                "w": 6,
                "x": 90,
                "y": 38
            }
        },
        {
            "Key": 80,
            "Value": {
                "id": "42e425ca-d99f-4e69-9c4d-211794969ca5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 80,
                "h": 10,
                "offset": 0,
                "shift": 8,
                "w": 6,
                "x": 82,
                "y": 38
            }
        },
        {
            "Key": 81,
            "Value": {
                "id": "00327a62-b4d0-4740-9700-827e210ddee1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 81,
                "h": 10,
                "offset": 0,
                "shift": 8,
                "w": 6,
                "x": 52,
                "y": 14
            }
        },
        {
            "Key": 82,
            "Value": {
                "id": "2154462b-7bd7-430f-b0fd-0675a6152b17",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 82,
                "h": 10,
                "offset": 0,
                "shift": 8,
                "w": 6,
                "x": 34,
                "y": 14
            }
        },
        {
            "Key": 83,
            "Value": {
                "id": "e6198e18-f8a3-4241-8409-2a0ab90b8325",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 83,
                "h": 10,
                "offset": 0,
                "shift": 8,
                "w": 6,
                "x": 26,
                "y": 14
            }
        },
        {
            "Key": 84,
            "Value": {
                "id": "a0466248-0088-4caa-8346-31aa5f012523",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 84,
                "h": 10,
                "offset": 0,
                "shift": 8,
                "w": 6,
                "x": 18,
                "y": 14
            }
        },
        {
            "Key": 85,
            "Value": {
                "id": "3846aeed-b5e9-4737-b253-7897c1ba7c97",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 85,
                "h": 10,
                "offset": 0,
                "shift": 8,
                "w": 6,
                "x": 10,
                "y": 14
            }
        },
        {
            "Key": 86,
            "Value": {
                "id": "12b5da8f-358e-478c-8323-b3826bbe0fa6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 86,
                "h": 10,
                "offset": 0,
                "shift": 8,
                "w": 6,
                "x": 2,
                "y": 14
            }
        },
        {
            "Key": 87,
            "Value": {
                "id": "d7f547b9-9b65-47bd-958a-bd833352c0bd",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 87,
                "h": 10,
                "offset": 0,
                "shift": 8,
                "w": 6,
                "x": 114,
                "y": 2
            }
        },
        {
            "Key": 88,
            "Value": {
                "id": "068a9b7f-9623-4c2f-be32-6b8059435f93",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 88,
                "h": 10,
                "offset": 0,
                "shift": 8,
                "w": 6,
                "x": 106,
                "y": 2
            }
        },
        {
            "Key": 89,
            "Value": {
                "id": "bcb64ab7-203e-4969-b977-c9d4ac9ab24a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 89,
                "h": 10,
                "offset": 0,
                "shift": 8,
                "w": 6,
                "x": 98,
                "y": 2
            }
        },
        {
            "Key": 90,
            "Value": {
                "id": "a19c5faa-fb51-4c9c-bf79-8e63cf652984",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 90,
                "h": 10,
                "offset": 0,
                "shift": 8,
                "w": 6,
                "x": 90,
                "y": 2
            }
        },
        {
            "Key": 91,
            "Value": {
                "id": "6595f3c3-5cdf-45be-baf0-5bebb5e4ccec",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 91,
                "h": 10,
                "offset": -1,
                "shift": 8,
                "w": 8,
                "x": 42,
                "y": 14
            }
        },
        {
            "Key": 92,
            "Value": {
                "id": "d99be4f9-0f43-46aa-b2d0-40aae8d95f2c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 92,
                "h": 10,
                "offset": 0,
                "shift": 5,
                "w": 4,
                "x": 84,
                "y": 2
            }
        },
        {
            "Key": 93,
            "Value": {
                "id": "56f59e95-8a0c-4c7c-9b8b-07eb0ab9706b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 93,
                "h": 10,
                "offset": 0,
                "shift": 8,
                "w": 6,
                "x": 68,
                "y": 2
            }
        },
        {
            "Key": 94,
            "Value": {
                "id": "fbbb1752-d332-45e5-ac2d-3f449279b830",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 94,
                "h": 10,
                "offset": 0,
                "shift": 5,
                "w": 5,
                "x": 61,
                "y": 2
            }
        },
        {
            "Key": 95,
            "Value": {
                "id": "75fee0e4-ae0a-4d33-ba78-db0e845377ab",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 95,
                "h": 10,
                "offset": -1,
                "shift": 5,
                "w": 6,
                "x": 53,
                "y": 2
            }
        },
        {
            "Key": 96,
            "Value": {
                "id": "276fc59c-025d-4f12-a301-646df201d412",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 96,
                "h": 10,
                "offset": 1,
                "shift": 5,
                "w": 3,
                "x": 48,
                "y": 2
            }
        },
        {
            "Key": 97,
            "Value": {
                "id": "a4ec88f2-f666-4b2f-a155-9d53e609a084",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 97,
                "h": 10,
                "offset": 0,
                "shift": 8,
                "w": 6,
                "x": 40,
                "y": 2
            }
        },
        {
            "Key": 98,
            "Value": {
                "id": "11e4eb0b-99bf-4125-99f6-ee31241892d0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 98,
                "h": 10,
                "offset": 0,
                "shift": 8,
                "w": 6,
                "x": 32,
                "y": 2
            }
        },
        {
            "Key": 99,
            "Value": {
                "id": "ee019409-9125-4ff4-834e-d298c4ce7b32",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 99,
                "h": 10,
                "offset": 0,
                "shift": 8,
                "w": 6,
                "x": 24,
                "y": 2
            }
        },
        {
            "Key": 100,
            "Value": {
                "id": "d8e9e70e-9f38-40f5-8b6d-c49dc99fd266",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 100,
                "h": 10,
                "offset": 0,
                "shift": 8,
                "w": 6,
                "x": 16,
                "y": 2
            }
        },
        {
            "Key": 101,
            "Value": {
                "id": "e727caac-88fe-46ac-ae33-9b8d923d4056",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 101,
                "h": 10,
                "offset": 0,
                "shift": 8,
                "w": 6,
                "x": 8,
                "y": 2
            }
        },
        {
            "Key": 102,
            "Value": {
                "id": "415e8c9e-4f05-4ed2-8956-d5002fb440dc",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 102,
                "h": 10,
                "offset": 0,
                "shift": 8,
                "w": 6,
                "x": 76,
                "y": 2
            }
        },
        {
            "Key": 103,
            "Value": {
                "id": "7a75a695-f371-4731-aeb0-6c83af59e946",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 103,
                "h": 10,
                "offset": 0,
                "shift": 8,
                "w": 6,
                "x": 60,
                "y": 14
            }
        },
        {
            "Key": 104,
            "Value": {
                "id": "10a075c6-69a3-46cf-979e-4cda492fce41",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 104,
                "h": 10,
                "offset": 0,
                "shift": 8,
                "w": 6,
                "x": 108,
                "y": 26
            }
        },
        {
            "Key": 105,
            "Value": {
                "id": "8340ea45-ba92-4f88-aecd-e0e8321b3f4e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 105,
                "h": 10,
                "offset": 1,
                "shift": 8,
                "w": 4,
                "x": 68,
                "y": 14
            }
        },
        {
            "Key": 106,
            "Value": {
                "id": "a513d695-a09b-4057-8334-3f4f38846cb4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 106,
                "h": 10,
                "offset": 0,
                "shift": 8,
                "w": 6,
                "x": 66,
                "y": 38
            }
        },
        {
            "Key": 107,
            "Value": {
                "id": "221e053f-7f12-4f95-9b2d-db66fa0fbb38",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 107,
                "h": 10,
                "offset": 0,
                "shift": 8,
                "w": 6,
                "x": 58,
                "y": 38
            }
        },
        {
            "Key": 108,
            "Value": {
                "id": "414738f9-612b-4501-88a9-8561785fa909",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 108,
                "h": 10,
                "offset": 0,
                "shift": 8,
                "w": 6,
                "x": 50,
                "y": 38
            }
        },
        {
            "Key": 109,
            "Value": {
                "id": "51017c11-c152-4758-b711-15924d23d428",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 109,
                "h": 10,
                "offset": 0,
                "shift": 8,
                "w": 6,
                "x": 42,
                "y": 38
            }
        },
        {
            "Key": 110,
            "Value": {
                "id": "aa046d56-9f18-4ab5-b9d1-6776e87fa0a8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 110,
                "h": 10,
                "offset": 0,
                "shift": 8,
                "w": 6,
                "x": 34,
                "y": 38
            }
        },
        {
            "Key": 111,
            "Value": {
                "id": "3592bb9e-9d23-4218-a940-752712d6419a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 111,
                "h": 10,
                "offset": 0,
                "shift": 8,
                "w": 6,
                "x": 26,
                "y": 38
            }
        },
        {
            "Key": 112,
            "Value": {
                "id": "5c5e7d72-e379-42f1-b613-e16e65626d7f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 112,
                "h": 10,
                "offset": 0,
                "shift": 8,
                "w": 6,
                "x": 18,
                "y": 38
            }
        },
        {
            "Key": 113,
            "Value": {
                "id": "29ac7c34-0d88-4521-bd13-9e6659f7d91a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 113,
                "h": 10,
                "offset": 0,
                "shift": 8,
                "w": 6,
                "x": 10,
                "y": 38
            }
        },
        {
            "Key": 114,
            "Value": {
                "id": "d897dee7-42e1-4174-8337-cab7fa5091c8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 114,
                "h": 10,
                "offset": 0,
                "shift": 8,
                "w": 6,
                "x": 2,
                "y": 38
            }
        },
        {
            "Key": 115,
            "Value": {
                "id": "2d57b69b-f9d4-4063-b558-a588d3e8b5ef",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 115,
                "h": 10,
                "offset": 0,
                "shift": 8,
                "w": 6,
                "x": 74,
                "y": 38
            }
        },
        {
            "Key": 116,
            "Value": {
                "id": "21dbf291-11b4-4a03-892e-a56e57889690",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 116,
                "h": 10,
                "offset": 0,
                "shift": 8,
                "w": 6,
                "x": 116,
                "y": 26
            }
        },
        {
            "Key": 117,
            "Value": {
                "id": "93d2d90d-83c7-4a20-80a7-9452089279bd",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 117,
                "h": 10,
                "offset": 0,
                "shift": 8,
                "w": 6,
                "x": 100,
                "y": 26
            }
        },
        {
            "Key": 118,
            "Value": {
                "id": "9735f953-0d7d-4587-a552-52a7d711c207",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 118,
                "h": 10,
                "offset": 0,
                "shift": 8,
                "w": 6,
                "x": 92,
                "y": 26
            }
        },
        {
            "Key": 119,
            "Value": {
                "id": "88851d5e-02af-40c9-a531-2ec028d569fb",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 119,
                "h": 10,
                "offset": 0,
                "shift": 8,
                "w": 6,
                "x": 84,
                "y": 26
            }
        },
        {
            "Key": 120,
            "Value": {
                "id": "4e8408a4-22a6-4174-94dd-eef9fcf3cc44",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 120,
                "h": 10,
                "offset": 0,
                "shift": 8,
                "w": 6,
                "x": 76,
                "y": 26
            }
        },
        {
            "Key": 121,
            "Value": {
                "id": "2a939c46-2bea-422c-80be-d18848f817c5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 121,
                "h": 10,
                "offset": 0,
                "shift": 8,
                "w": 6,
                "x": 68,
                "y": 26
            }
        },
        {
            "Key": 122,
            "Value": {
                "id": "25308cfd-d798-4a3b-bedf-827ff6a86867",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 122,
                "h": 10,
                "offset": 0,
                "shift": 8,
                "w": 6,
                "x": 60,
                "y": 26
            }
        },
        {
            "Key": 123,
            "Value": {
                "id": "b2cdb9ff-6eb6-40bb-a0a5-6ef678612d7b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 123,
                "h": 10,
                "offset": 0,
                "shift": 5,
                "w": 4,
                "x": 54,
                "y": 26
            }
        },
        {
            "Key": 124,
            "Value": {
                "id": "ee08bd5a-ad2d-4e64-b0e0-9fdb366d2efc",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 124,
                "h": 10,
                "offset": 0,
                "shift": 50,
                "w": 50,
                "x": 2,
                "y": 26
            }
        },
        {
            "Key": 125,
            "Value": {
                "id": "bad5e83b-d660-4225-a4bf-c5fd770a79dd",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 125,
                "h": 10,
                "offset": 0,
                "shift": 5,
                "w": 4,
                "x": 74,
                "y": 14
            }
        },
        {
            "Key": 126,
            "Value": {
                "id": "8e854574-6383-4ab7-b285-cd462f69167a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 126,
                "h": 10,
                "offset": 0,
                "shift": 8,
                "w": 4,
                "x": 64,
                "y": 74
            }
        },
        {
            "Key": 9647,
            "Value": {
                "id": "f451cba3-0f7c-45df-a7bf-6c9c3b8ab42a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 9647,
                "h": 10,
                "offset": 1,
                "shift": 8,
                "w": 6,
                "x": 70,
                "y": 74
            }
        }
    ],
    "hinting": 0,
    "includeTTF": true,
    "interpreter": 0,
    "italic": false,
    "kerningPairs": [
        
    ],
    "last": 0,
    "maintainGms1Font": false,
    "pointRounding": 0,
    "ranges": [
        {
            "x": 32,
            "y": 127
        },
        {
            "x": 9647,
            "y": 9647
        }
    ],
    "sampleText": "abcdef ABCDEF\\u000a0123456789 .,<>\"'&!?\\u000athe quick brown fox jumps over the lazy dog\\u000aTHE QUICK BROWN FOX JUMPS OVER THE LAZY DOG\\u000aDefault character: ▯ (9647)",
    "size": 6,
    "styleName": "Regular",
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f"
}
{
    "id": "e4ffd73c-b60d-489d-8787-f634a65a499a",
    "modelName": "GMFont",
    "mvc": "1.1",
    "name": "fnt_gb_small",
    "AntiAlias": 1,
    "TTFName": "${project_dir}\\fonts\\fnt_gb_small\\Early GameBoy.ttf",
    "ascenderOffset": 0,
    "bold": false,
    "charset": 0,
    "first": 0,
    "fontName": "Early GameBoy",
    "glyphOperations": 0,
    "glyphs": [
        {
            "Key": 32,
            "Value": {
                "id": "ad1d5c85-01a2-49e2-98e5-826efbc45de0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 32,
                "h": 14,
                "offset": 0,
                "shift": 5,
                "w": 5,
                "x": 2,
                "y": 2
            }
        },
        {
            "Key": 33,
            "Value": {
                "id": "6c5d2a65-03cb-49c7-b91e-e36198f40630",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 33,
                "h": 14,
                "offset": 3,
                "shift": 11,
                "w": 4,
                "x": 217,
                "y": 34
            }
        },
        {
            "Key": 34,
            "Value": {
                "id": "e6e215c4-3569-41ff-931e-a2a8b1f0300c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 34,
                "h": 14,
                "offset": 0,
                "shift": 11,
                "w": 8,
                "x": 207,
                "y": 34
            }
        },
        {
            "Key": 35,
            "Value": {
                "id": "a016b180-62d1-44d6-88ae-9b69014d6cf7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 35,
                "h": 14,
                "offset": 0,
                "shift": 7,
                "w": 7,
                "x": 198,
                "y": 34
            }
        },
        {
            "Key": 36,
            "Value": {
                "id": "2d278030-e2b4-47b0-976b-0307b5f12bab",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 36,
                "h": 14,
                "offset": 0,
                "shift": 7,
                "w": 6,
                "x": 190,
                "y": 34
            }
        },
        {
            "Key": 37,
            "Value": {
                "id": "0aacb4db-9cc6-4462-baec-1b72c4b49e6f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 37,
                "h": 14,
                "offset": 0,
                "shift": 7,
                "w": 7,
                "x": 181,
                "y": 34
            }
        },
        {
            "Key": 38,
            "Value": {
                "id": "62d79316-07ed-4117-8ce7-ea67dcddb4df",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 38,
                "h": 14,
                "offset": 0,
                "shift": 7,
                "w": 7,
                "x": 172,
                "y": 34
            }
        },
        {
            "Key": 39,
            "Value": {
                "id": "6caccc6b-2d43-4c7e-9a18-2ef9b8d1eae2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 39,
                "h": 14,
                "offset": 2,
                "shift": 10,
                "w": 3,
                "x": 167,
                "y": 34
            }
        },
        {
            "Key": 40,
            "Value": {
                "id": "263a9131-9fc7-4ecf-ba54-9d5b1f07e53b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 40,
                "h": 14,
                "offset": -2,
                "shift": 11,
                "w": 10,
                "x": 155,
                "y": 34
            }
        },
        {
            "Key": 41,
            "Value": {
                "id": "a23ee2f6-9621-469a-8ac4-a3b4ce6e25fa",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 41,
                "h": 14,
                "offset": -1,
                "shift": 10,
                "w": 7,
                "x": 146,
                "y": 34
            }
        },
        {
            "Key": 42,
            "Value": {
                "id": "43a9bba1-5d2c-406e-bdbc-0320dbc575eb",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 42,
                "h": 14,
                "offset": -1,
                "shift": 10,
                "w": 7,
                "x": 223,
                "y": 34
            }
        },
        {
            "Key": 43,
            "Value": {
                "id": "d2dfbe5e-4b2d-4f51-9c37-51654bb9d4e1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 43,
                "h": 14,
                "offset": 0,
                "shift": 7,
                "w": 6,
                "x": 138,
                "y": 34
            }
        },
        {
            "Key": 44,
            "Value": {
                "id": "fe649fa4-262e-46cb-aaf7-c37dd2713a22",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 44,
                "h": 14,
                "offset": 0,
                "shift": 10,
                "w": 3,
                "x": 122,
                "y": 34
            }
        },
        {
            "Key": 45,
            "Value": {
                "id": "e5b7c19b-93c0-42db-b37e-1bcd36819758",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 45,
                "h": 14,
                "offset": 1,
                "shift": 10,
                "w": 5,
                "x": 115,
                "y": 34
            }
        },
        {
            "Key": 46,
            "Value": {
                "id": "834f881c-3029-49cc-aade-83279edf5aac",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 46,
                "h": 14,
                "offset": 0,
                "shift": 11,
                "w": 3,
                "x": 110,
                "y": 34
            }
        },
        {
            "Key": 47,
            "Value": {
                "id": "10e56df9-d682-4a29-b812-edcd72550df7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 47,
                "h": 14,
                "offset": 1,
                "shift": 7,
                "w": 5,
                "x": 103,
                "y": 34
            }
        },
        {
            "Key": 48,
            "Value": {
                "id": "bbacff08-e217-48d1-95a6-2fdc758a56e7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 48,
                "h": 14,
                "offset": 0,
                "shift": 11,
                "w": 8,
                "x": 93,
                "y": 34
            }
        },
        {
            "Key": 49,
            "Value": {
                "id": "6401e899-ac28-41c3-98bb-7a016bb7c0de",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 49,
                "h": 14,
                "offset": 1,
                "shift": 10,
                "w": 5,
                "x": 86,
                "y": 34
            }
        },
        {
            "Key": 50,
            "Value": {
                "id": "53ad721e-c085-4b74-a6b0-7fbee62f6ca1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 50,
                "h": 14,
                "offset": -1,
                "shift": 11,
                "w": 9,
                "x": 75,
                "y": 34
            }
        },
        {
            "Key": 51,
            "Value": {
                "id": "1f0c8696-2ae7-4d00-b0fe-66f5e08ec834",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 51,
                "h": 14,
                "offset": 0,
                "shift": 11,
                "w": 8,
                "x": 65,
                "y": 34
            }
        },
        {
            "Key": 52,
            "Value": {
                "id": "65252237-df9f-43cf-8b51-5205fbe4084c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 52,
                "h": 14,
                "offset": -1,
                "shift": 11,
                "w": 9,
                "x": 54,
                "y": 34
            }
        },
        {
            "Key": 53,
            "Value": {
                "id": "ae491c26-841d-42db-8d7c-3537521c1e2a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 53,
                "h": 14,
                "offset": -1,
                "shift": 11,
                "w": 9,
                "x": 127,
                "y": 34
            }
        },
        {
            "Key": 54,
            "Value": {
                "id": "33790762-e8c4-4ace-a089-e35ea629e38c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 54,
                "h": 14,
                "offset": 0,
                "shift": 11,
                "w": 8,
                "x": 232,
                "y": 34
            }
        },
        {
            "Key": 55,
            "Value": {
                "id": "11eb8466-154a-4b39-b632-a9a1822bfa3f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 55,
                "h": 14,
                "offset": 0,
                "shift": 11,
                "w": 9,
                "x": 242,
                "y": 34
            }
        },
        {
            "Key": 56,
            "Value": {
                "id": "597ef468-6619-4aa6-ae63-fdca664e541a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 56,
                "h": 14,
                "offset": -1,
                "shift": 11,
                "w": 9,
                "x": 2,
                "y": 50
            }
        },
        {
            "Key": 57,
            "Value": {
                "id": "286f2e60-b6fe-45f8-a773-80c031a103bc",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 57,
                "h": 14,
                "offset": -1,
                "shift": 11,
                "w": 9,
                "x": 204,
                "y": 50
            }
        },
        {
            "Key": 58,
            "Value": {
                "id": "110cb631-df19-45cc-808a-63a0779e9b2f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 58,
                "h": 14,
                "offset": 1,
                "shift": 10,
                "w": 3,
                "x": 199,
                "y": 50
            }
        },
        {
            "Key": 59,
            "Value": {
                "id": "83e79ff8-f0b0-4487-8863-3163b3e05f35",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 59,
                "h": 14,
                "offset": 0,
                "shift": 10,
                "w": 3,
                "x": 194,
                "y": 50
            }
        },
        {
            "Key": 60,
            "Value": {
                "id": "e4abdc4e-83e2-4529-a23f-74f4ee2f3a51",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 60,
                "h": 14,
                "offset": -1,
                "shift": 12,
                "w": 10,
                "x": 182,
                "y": 50
            }
        },
        {
            "Key": 61,
            "Value": {
                "id": "4096a780-0065-4255-ba09-ae9e1241ee45",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 61,
                "h": 14,
                "offset": 0,
                "shift": 7,
                "w": 6,
                "x": 174,
                "y": 50
            }
        },
        {
            "Key": 62,
            "Value": {
                "id": "24483af3-bed4-4e6f-8354-88cefe39a5e4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 62,
                "h": 14,
                "offset": 1,
                "shift": 12,
                "w": 7,
                "x": 165,
                "y": 50
            }
        },
        {
            "Key": 63,
            "Value": {
                "id": "e2836d50-be6a-4027-af26-938351b2d233",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 63,
                "h": 14,
                "offset": -2,
                "shift": 11,
                "w": 9,
                "x": 154,
                "y": 50
            }
        },
        {
            "Key": 64,
            "Value": {
                "id": "4e93956d-94fc-4ff6-9f40-eb683eb20a47",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 64,
                "h": 14,
                "offset": 0,
                "shift": 7,
                "w": 7,
                "x": 145,
                "y": 50
            }
        },
        {
            "Key": 65,
            "Value": {
                "id": "ad567cca-1eae-4fbd-b373-9f0dca759173",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 65,
                "h": 14,
                "offset": -1,
                "shift": 11,
                "w": 9,
                "x": 134,
                "y": 50
            }
        },
        {
            "Key": 66,
            "Value": {
                "id": "3714b94d-acdd-45da-9397-6c3b5be46a13",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 66,
                "h": 14,
                "offset": 0,
                "shift": 11,
                "w": 8,
                "x": 124,
                "y": 50
            }
        },
        {
            "Key": 67,
            "Value": {
                "id": "3d263d5a-9646-44f6-913a-394287f9eadc",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 67,
                "h": 14,
                "offset": 0,
                "shift": 11,
                "w": 8,
                "x": 114,
                "y": 50
            }
        },
        {
            "Key": 68,
            "Value": {
                "id": "3b97d626-fcd2-4d6f-a98b-c3e0f33b5325",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 68,
                "h": 14,
                "offset": -1,
                "shift": 11,
                "w": 9,
                "x": 103,
                "y": 50
            }
        },
        {
            "Key": 69,
            "Value": {
                "id": "6c6259b0-e5ee-4980-b798-a8254104eda7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 69,
                "h": 14,
                "offset": 0,
                "shift": 11,
                "w": 8,
                "x": 93,
                "y": 50
            }
        },
        {
            "Key": 70,
            "Value": {
                "id": "2456f47c-44d4-4972-8c82-2a8c8a39bf12",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 70,
                "h": 14,
                "offset": 0,
                "shift": 11,
                "w": 8,
                "x": 83,
                "y": 50
            }
        },
        {
            "Key": 71,
            "Value": {
                "id": "0b414c4f-3bbc-46f6-8c48-7645f00a2df3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 71,
                "h": 14,
                "offset": 0,
                "shift": 11,
                "w": 8,
                "x": 73,
                "y": 50
            }
        },
        {
            "Key": 72,
            "Value": {
                "id": "c075483b-6ebc-4ba0-bf42-7559b5098765",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 72,
                "h": 14,
                "offset": -1,
                "shift": 11,
                "w": 9,
                "x": 62,
                "y": 50
            }
        },
        {
            "Key": 73,
            "Value": {
                "id": "2986c9ae-47a8-453d-9770-cc7716c6c7f7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 73,
                "h": 14,
                "offset": 1,
                "shift": 10,
                "w": 5,
                "x": 55,
                "y": 50
            }
        },
        {
            "Key": 74,
            "Value": {
                "id": "5e6b79a1-9b1a-4775-b672-e0645cc2eecf",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 74,
                "h": 14,
                "offset": 0,
                "shift": 11,
                "w": 8,
                "x": 45,
                "y": 50
            }
        },
        {
            "Key": 75,
            "Value": {
                "id": "4ee712c2-16d9-4399-aaad-874bb1351513",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 75,
                "h": 14,
                "offset": 0,
                "shift": 11,
                "w": 8,
                "x": 35,
                "y": 50
            }
        },
        {
            "Key": 76,
            "Value": {
                "id": "08ca250d-c0aa-4627-968d-f281568345a0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 76,
                "h": 14,
                "offset": 0,
                "shift": 11,
                "w": 8,
                "x": 25,
                "y": 50
            }
        },
        {
            "Key": 77,
            "Value": {
                "id": "f0e6249a-a858-4fc8-9a88-c3abec63bf03",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 77,
                "h": 14,
                "offset": -1,
                "shift": 11,
                "w": 10,
                "x": 13,
                "y": 50
            }
        },
        {
            "Key": 78,
            "Value": {
                "id": "8aa1b188-4320-4cd7-89d9-4c0c121e0a3f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 78,
                "h": 14,
                "offset": -1,
                "shift": 11,
                "w": 9,
                "x": 43,
                "y": 34
            }
        },
        {
            "Key": 79,
            "Value": {
                "id": "3e2443b8-a17e-4df1-ae09-0cfcf5bc1a0e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 79,
                "h": 14,
                "offset": 0,
                "shift": 11,
                "w": 8,
                "x": 33,
                "y": 34
            }
        },
        {
            "Key": 80,
            "Value": {
                "id": "207836b6-a61b-4953-9599-87a5b440fc88",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 80,
                "h": 14,
                "offset": 0,
                "shift": 11,
                "w": 8,
                "x": 23,
                "y": 34
            }
        },
        {
            "Key": 81,
            "Value": {
                "id": "988999b1-66fa-4fff-9a2a-74d29ff7c1c3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 81,
                "h": 14,
                "offset": 0,
                "shift": 11,
                "w": 8,
                "x": 220,
                "y": 2
            }
        },
        {
            "Key": 82,
            "Value": {
                "id": "59cad3c7-7557-4dd4-b67d-32a6371608ec",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 82,
                "h": 14,
                "offset": 0,
                "shift": 11,
                "w": 8,
                "x": 197,
                "y": 2
            }
        },
        {
            "Key": 83,
            "Value": {
                "id": "25abc879-9965-465a-891b-491206b57a5c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 83,
                "h": 14,
                "offset": -1,
                "shift": 11,
                "w": 9,
                "x": 186,
                "y": 2
            }
        },
        {
            "Key": 84,
            "Value": {
                "id": "78ff8120-cb13-4fbd-b4a7-fea81074f1fd",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 84,
                "h": 14,
                "offset": 0,
                "shift": 11,
                "w": 8,
                "x": 176,
                "y": 2
            }
        },
        {
            "Key": 85,
            "Value": {
                "id": "87afb5bd-a502-4355-8a7f-61955ec7b81f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 85,
                "h": 14,
                "offset": -1,
                "shift": 11,
                "w": 9,
                "x": 165,
                "y": 2
            }
        },
        {
            "Key": 86,
            "Value": {
                "id": "86ab3cea-c03c-4a8b-9517-2b6e9a17ff3b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 86,
                "h": 14,
                "offset": -1,
                "shift": 11,
                "w": 9,
                "x": 154,
                "y": 2
            }
        },
        {
            "Key": 87,
            "Value": {
                "id": "8c3631fe-6c1c-47a6-8b61-e306fb2e8ae2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 87,
                "h": 14,
                "offset": -1,
                "shift": 11,
                "w": 10,
                "x": 142,
                "y": 2
            }
        },
        {
            "Key": 88,
            "Value": {
                "id": "3ecfe157-b015-4a40-a96e-ca86e86ed643",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 88,
                "h": 14,
                "offset": -1,
                "shift": 11,
                "w": 9,
                "x": 131,
                "y": 2
            }
        },
        {
            "Key": 89,
            "Value": {
                "id": "0b99314b-006a-4481-8856-b8a398b2ca48",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 89,
                "h": 14,
                "offset": 0,
                "shift": 11,
                "w": 8,
                "x": 121,
                "y": 2
            }
        },
        {
            "Key": 90,
            "Value": {
                "id": "a143bcbb-5e93-4406-b700-3d7498878193",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 90,
                "h": 14,
                "offset": 0,
                "shift": 11,
                "w": 8,
                "x": 111,
                "y": 2
            }
        },
        {
            "Key": 91,
            "Value": {
                "id": "8c4a6072-128c-4208-a1e1-307897b10198",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 91,
                "h": 14,
                "offset": -2,
                "shift": 10,
                "w": 11,
                "x": 207,
                "y": 2
            }
        },
        {
            "Key": 92,
            "Value": {
                "id": "f5f56a4f-806d-473c-b10d-4fe029658626",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 92,
                "h": 14,
                "offset": 1,
                "shift": 7,
                "w": 5,
                "x": 104,
                "y": 2
            }
        },
        {
            "Key": 93,
            "Value": {
                "id": "81cfbc35-801e-4048-8556-399c36c78554",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 93,
                "h": 14,
                "offset": 0,
                "shift": 11,
                "w": 8,
                "x": 84,
                "y": 2
            }
        },
        {
            "Key": 94,
            "Value": {
                "id": "67382846-edc8-4f08-8b2b-fb5d565ee052",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 94,
                "h": 14,
                "offset": 0,
                "shift": 7,
                "w": 6,
                "x": 76,
                "y": 2
            }
        },
        {
            "Key": 95,
            "Value": {
                "id": "fbe297f3-d9b2-47cf-8291-b22a62b1e3c1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 95,
                "h": 14,
                "offset": -1,
                "shift": 7,
                "w": 8,
                "x": 66,
                "y": 2
            }
        },
        {
            "Key": 96,
            "Value": {
                "id": "d76f4385-32f3-4902-bc3d-4eda41f60f52",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 96,
                "h": 14,
                "offset": 2,
                "shift": 7,
                "w": 3,
                "x": 61,
                "y": 2
            }
        },
        {
            "Key": 97,
            "Value": {
                "id": "d64c214f-2849-45e3-8ec6-04b5892f8cba",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 97,
                "h": 14,
                "offset": -1,
                "shift": 11,
                "w": 9,
                "x": 50,
                "y": 2
            }
        },
        {
            "Key": 98,
            "Value": {
                "id": "181da2a5-d982-4d1f-8d4c-e8b4ad9ba493",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 98,
                "h": 14,
                "offset": 0,
                "shift": 11,
                "w": 8,
                "x": 40,
                "y": 2
            }
        },
        {
            "Key": 99,
            "Value": {
                "id": "d607ecd5-5462-49df-93a4-1c246b073311",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 99,
                "h": 14,
                "offset": 0,
                "shift": 11,
                "w": 8,
                "x": 30,
                "y": 2
            }
        },
        {
            "Key": 100,
            "Value": {
                "id": "cfda9ae3-f226-44a1-be40-b59e99206397",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 100,
                "h": 14,
                "offset": -1,
                "shift": 11,
                "w": 9,
                "x": 19,
                "y": 2
            }
        },
        {
            "Key": 101,
            "Value": {
                "id": "1b00e552-c969-4486-b021-cd7ff1516d5d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 101,
                "h": 14,
                "offset": 0,
                "shift": 11,
                "w": 8,
                "x": 9,
                "y": 2
            }
        },
        {
            "Key": 102,
            "Value": {
                "id": "413397df-ec97-426e-b16b-a4cdb57e53c1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 102,
                "h": 14,
                "offset": 0,
                "shift": 11,
                "w": 8,
                "x": 94,
                "y": 2
            }
        },
        {
            "Key": 103,
            "Value": {
                "id": "1cdf3f58-1b68-41d8-9fdb-8ba9c1a55c57",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 103,
                "h": 14,
                "offset": 0,
                "shift": 11,
                "w": 8,
                "x": 230,
                "y": 2
            }
        },
        {
            "Key": 104,
            "Value": {
                "id": "a1d968b8-8a98-4962-9a33-c4fa489753d8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 104,
                "h": 14,
                "offset": -1,
                "shift": 11,
                "w": 9,
                "x": 143,
                "y": 18
            }
        },
        {
            "Key": 105,
            "Value": {
                "id": "24c3dd1e-53b8-4a91-989c-ad72129eaf6b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 105,
                "h": 14,
                "offset": 1,
                "shift": 10,
                "w": 5,
                "x": 240,
                "y": 2
            }
        },
        {
            "Key": 106,
            "Value": {
                "id": "52407efe-08a7-4bb1-b2a4-3aef1c979ba6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 106,
                "h": 14,
                "offset": 0,
                "shift": 11,
                "w": 8,
                "x": 2,
                "y": 34
            }
        },
        {
            "Key": 107,
            "Value": {
                "id": "a4239175-5302-4a6e-9376-52a1d79ab2a9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 107,
                "h": 14,
                "offset": 0,
                "shift": 11,
                "w": 8,
                "x": 237,
                "y": 18
            }
        },
        {
            "Key": 108,
            "Value": {
                "id": "7852c2ed-13e6-443e-9cd6-5c8f7f93af47",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 108,
                "h": 14,
                "offset": 0,
                "shift": 11,
                "w": 8,
                "x": 227,
                "y": 18
            }
        },
        {
            "Key": 109,
            "Value": {
                "id": "6e8b839f-d4da-47aa-a5b5-fd6feeccd975",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 109,
                "h": 14,
                "offset": -1,
                "shift": 11,
                "w": 10,
                "x": 215,
                "y": 18
            }
        },
        {
            "Key": 110,
            "Value": {
                "id": "fda6fa52-e222-49ff-b856-dacf0c085f76",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 110,
                "h": 14,
                "offset": -1,
                "shift": 11,
                "w": 9,
                "x": 204,
                "y": 18
            }
        },
        {
            "Key": 111,
            "Value": {
                "id": "479a20bb-6c18-4369-ba62-875f42eec406",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 111,
                "h": 14,
                "offset": 0,
                "shift": 11,
                "w": 8,
                "x": 194,
                "y": 18
            }
        },
        {
            "Key": 112,
            "Value": {
                "id": "1b5142dd-d48b-4a3f-a975-a4883465e944",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 112,
                "h": 14,
                "offset": 0,
                "shift": 11,
                "w": 8,
                "x": 184,
                "y": 18
            }
        },
        {
            "Key": 113,
            "Value": {
                "id": "fede2e48-8531-490a-afb8-df05586a9d6d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 113,
                "h": 14,
                "offset": 0,
                "shift": 11,
                "w": 8,
                "x": 174,
                "y": 18
            }
        },
        {
            "Key": 114,
            "Value": {
                "id": "ff51be5d-99b0-464f-8379-6ec95e28e6b5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 114,
                "h": 14,
                "offset": 0,
                "shift": 11,
                "w": 8,
                "x": 164,
                "y": 18
            }
        },
        {
            "Key": 115,
            "Value": {
                "id": "d518ab4f-6c7d-4b77-b089-b7ee57b89f67",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 115,
                "h": 14,
                "offset": -1,
                "shift": 11,
                "w": 9,
                "x": 12,
                "y": 34
            }
        },
        {
            "Key": 116,
            "Value": {
                "id": "956d55ff-f770-4600-b7c5-5b3c71699baf",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 116,
                "h": 14,
                "offset": 0,
                "shift": 11,
                "w": 8,
                "x": 154,
                "y": 18
            }
        },
        {
            "Key": 117,
            "Value": {
                "id": "f955bc39-6d37-44be-a006-636e99fc605e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 117,
                "h": 14,
                "offset": -1,
                "shift": 11,
                "w": 9,
                "x": 132,
                "y": 18
            }
        },
        {
            "Key": 118,
            "Value": {
                "id": "0b80e85f-f1bf-4f0d-9804-90002c4c6aef",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 118,
                "h": 14,
                "offset": -1,
                "shift": 11,
                "w": 9,
                "x": 121,
                "y": 18
            }
        },
        {
            "Key": 119,
            "Value": {
                "id": "491c3066-bafc-4f5d-ae70-643ac905f44f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 119,
                "h": 14,
                "offset": -1,
                "shift": 11,
                "w": 10,
                "x": 109,
                "y": 18
            }
        },
        {
            "Key": 120,
            "Value": {
                "id": "235cf3c6-0981-493f-84a6-79d63ce15527",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 120,
                "h": 14,
                "offset": -1,
                "shift": 11,
                "w": 9,
                "x": 98,
                "y": 18
            }
        },
        {
            "Key": 121,
            "Value": {
                "id": "3ffcaf46-146f-4223-8d2c-763567cc81f1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 121,
                "h": 14,
                "offset": 0,
                "shift": 11,
                "w": 8,
                "x": 88,
                "y": 18
            }
        },
        {
            "Key": 122,
            "Value": {
                "id": "ad3fa291-6915-4bf0-b848-66874d683728",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 122,
                "h": 14,
                "offset": 0,
                "shift": 11,
                "w": 8,
                "x": 78,
                "y": 18
            }
        },
        {
            "Key": 123,
            "Value": {
                "id": "41624d4a-79c7-48b3-b805-4f0f7946c829",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 123,
                "h": 14,
                "offset": 1,
                "shift": 7,
                "w": 5,
                "x": 71,
                "y": 18
            }
        },
        {
            "Key": 124,
            "Value": {
                "id": "b77f81db-3afb-47b7-9932-5f2905fe0410",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 124,
                "h": 14,
                "offset": 0,
                "shift": 67,
                "w": 67,
                "x": 2,
                "y": 18
            }
        },
        {
            "Key": 125,
            "Value": {
                "id": "71ddabf7-5aae-4a48-b121-dec58d669a3e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 125,
                "h": 14,
                "offset": 1,
                "shift": 7,
                "w": 5,
                "x": 247,
                "y": 2
            }
        },
        {
            "Key": 126,
            "Value": {
                "id": "d5005f65-3701-470f-b5a1-52e903b94df5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 126,
                "h": 14,
                "offset": 0,
                "shift": 11,
                "w": 6,
                "x": 215,
                "y": 50
            }
        },
        {
            "Key": 9647,
            "Value": {
                "id": "3db9ee2e-324a-4ef4-a0a6-2c5bc22be572",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 9647,
                "h": 14,
                "offset": 2,
                "shift": 11,
                "w": 7,
                "x": 223,
                "y": 50
            }
        }
    ],
    "hinting": 0,
    "includeTTF": true,
    "interpreter": 0,
    "italic": false,
    "kerningPairs": [
        
    ],
    "last": 0,
    "maintainGms1Font": false,
    "pointRounding": 0,
    "ranges": [
        {
            "x": 32,
            "y": 127
        },
        {
            "x": 9647,
            "y": 9647
        }
    ],
    "sampleText": "abcdef ABCDEF\\u000a0123456789 .,<>\"'&!?\\u000athe quick brown fox jumps over the lazy dog\\u000aTHE QUICK BROWN FOX JUMPS OVER THE LAZY DOG\\u000aDefault character: ▯ (9647)",
    "size": 8,
    "styleName": "Regular",
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f"
}